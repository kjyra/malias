<?php

namespace backend\components;

use yii\base\BootstrapInterface;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;

/**
 * Description of CRUDActionColumnFontAwesome
 *
 * @author grigo
 */
class CRUDActionColumnFontAwesome implements BootstrapInterface
{
    //put your code here
    public function bootstrap($app)
    {
        $actionColumnSetting = [
            'buttons' => [
                'view' => function($name, $model, $key){
                    return Html::a('<i class="fas fa-eye" aria-hidden="true"></i>', ['view', 'id' => $model->id]);
                },
                'update' => function($name, $model, $key){
                    return Html::a('<i class="fas fa-pencil-alt" aria-hidden="true"></i>', ['update', 'id' => $model->id]);
                },
                'delete' => function($name, $model, $key){
                    return Html::a('<i class="fas fa-trash" aria-hidden="true"></i>', ['delete', 'id' => $model->id],
                            ['data-method'=>'post', 'class' => 'delete-element']);
                }
            ],
        ];
        \Yii::$container->set(ActionColumn::class, $actionColumnSetting);
    }

}
