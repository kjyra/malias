<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string $title
 * @property string|null $description
 * @property string|null $image
 * @property float|null $price
 * @property float|null $old_price
 * @property int|null $is_new
 * @property int|null $is_hot
 * @property int|null $status
 * @property string|null $secondary_img
 * @property string|null $slider
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $bestseller
 */
class Product extends \yii\db\ActiveRecord
{
    
    const DEFAULT_IMAGE = 'img/product/imgonline-com-ua-Resize-PzB9NjQZsU.jpg';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'is_new', 'is_hot', 'status', 'created_at', 'updated_at', 'bestseller'], 'integer'],
            [['title'], 'required'],
            [['description', 'slider'], 'string'],
            [['price', 'old_price'], 'number'],
            [['title', 'image', 'secondary_img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'is_new' => 'Is New',
            'is_hot' => 'Is Hot',
            'status' => 'Status',
            'secondary_img' => 'Secondary Img',
            'slider' => 'Slider',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'bestseller' => 'Bestseller',
        ];
    }
    
    public function getImage()
    {
        if($this->image) {
            return 'http://malias.com/' . $this->image;
        }
        return 'http://malias.com/' . self::DEFAULT_IMAGE;
    }
}
