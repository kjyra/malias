<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $qty
 * @property float|null $total
 * @property int|null $status
 * @property string|null $name
 * @property string|null $email
 * @property string|null $number
 * @property string|null $text
 * @property string|null $address
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'qty', 'status'], 'integer'],
            [['total'], 'number'],
            [['text'], 'string'],
            [['name', 'email', 'number', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'qty' => 'Qty',
            'total' => 'Total',
            'status' => 'Status',
            'name' => 'Name',
            'email' => 'Email',
            'number' => 'Number',
            'text' => 'Text',
            'address' => 'Address',
        ];
    }
    
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::class, ['order_id' => 'id']);
    }
}
