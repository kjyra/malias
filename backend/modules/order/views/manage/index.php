<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'user_id',
            'created_at:date',
            'updated_at:date',
//            'qty',
            'total',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($order) {
                   return ($order->status) ? '<span class="text-danger">завершен</span>' : '<span class="text-success">новый</span>';
                }
            ],
//            'name',
            'email:email',
            'number',
            'text:ntext',
            'address',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;{delete}',
            ],

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
