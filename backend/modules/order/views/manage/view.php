<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Order */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'created_at:datetime',
            'updated_at:datetime',
            'qty',
            'total',
            'status',
            'name',
            'email:email',
            'number',
            'text:ntext',
            'address',
        ],
    ]) ?>

</div>

<div class="row">
    <div class="col-md-12">
        <table style="width: 100%;">
                <thead style="background: #ccc">
                    <tr>
                        <td>Id</td>
                        <td>Title</td>
                        <td>Price</td>
                        <td>Total</td>
                        <td>Qty</td>
                    </tr> 
                </thead>
                <tbody style="background: #ddd">
        <?php foreach($model->orderProducts as $product): ?>
                    <tr>
                        <td><?php echo $product->id; ?></td>
                        <td><?php echo $product->title; ?></td>
                        <td><?php echo $product->price; ?></td>
                        <td><?php echo $product->total; ?></td>
                        <td><?php echo $product->qty; ?></td>
                    </tr> 
        <?php endforeach; ?>
                </tbody>
            </table>
    </div>
</div>
