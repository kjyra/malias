<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'format' => 'raw',
                'value' => function($product) {
                    return Html::a($product->id, ['view', 'id' => $product->id]);
                }
            ],
            [
                'attribute' => 'category_id',
                'format' => 'raw',
                'value' => function($product) {
                    return Html::a($product->category_id, ['/category/manage/view', 'id' => $product->category_id]);
                }
            ],
            'title',
            'description:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($product) {
                    return Html::img($product->getImage(), ['alt' => 'product', 'width' => '100px']);
                }
            ],
            'price',
            'old_price',
            'is_new',
            [
                'attribute' => 'is_hot',
                'format' => 'raw',
                'value' => function($product) {
                    return ($product->is_hot) ? 'yes' : 'no';
                }
            ],
            'status',
            //'secondary_img',
            //'slider:ntext',
            //'created_at',
            //'updated_at',
            //'bestseller',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;{delete}',
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
