<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attribute_product}}`.
 */
class m210401_173133_create_attribute_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attribute_product}}', [
           'attr_value_id' => $this->integer(),
           'product_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%attribute_product}}');
    }
}
