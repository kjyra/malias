<?php

use yii\db\Migration;

/**
 * Class m210310_185128_add_index_product_title
 */
class m210310_185128_add_index_product_title extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE product ADD FULLTEXT INDEX idx_title (title)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_title', '{{%product}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210310_185128_add_index_product_title cannot be reverted.\n";

        return false;
    }
    */
}
