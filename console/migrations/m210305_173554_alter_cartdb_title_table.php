<?php

use yii\db\Migration;

/**
 * Class m210305_173554_alter_cartdb_title_table
 */
class m210305_173554_alter_cartdb_title_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cartdb}}', 'title', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cartdb}}', 'title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210305_173554_alter_cartdb_title_table cannot be reverted.\n";

        return false;
    }
    */
}
