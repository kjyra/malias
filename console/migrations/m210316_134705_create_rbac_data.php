<?php

use yii\db\Migration;
use backend\models\User;

/**
 * Class m210316_134705_create_rbac_data
 */
class m210316_134705_create_rbac_data extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $auth = Yii::$app->authManager;
        // Define permissions
        $viewOrdersListPermission = $auth->createPermission('viewOrdersList');
        $auth->add($viewOrdersListPermission);
        $viewOrderPermission = $auth->createPermission('viewOrder');
        $auth->add($viewOrderPermission);
        $deleteOrderPermission = $auth->createPermission('deleteOrder');
        $auth->add($deleteOrderPermission);
        $updateOrderPermission = $auth->createPermission('updateOrder');
        $auth->add($updateOrderPermission);
        
        $viewUsersListPermission = $auth->createPermission('viewUsersList');
        $auth->add($viewUsersListPermission);
        $viewUserPermission = $auth->createPermission('viewUser');
        $auth->add($viewUserPermission);
        $deleteUserPermission = $auth->createPermission('deleteUser');
        $auth->add($deleteUserPermission);
        $updateUserPermission = $auth->createPermission('updateUser');
        $auth->add($updateUserPermission);
        
        $viewProductsListPermission = $auth->createPermission('viewProductsList');
        $auth->add($viewProductsListPermission);
        $viewProductPermission = $auth->createPermission('viewProduct');
        $auth->add($viewProductPermission);
        $deleteProductPermission = $auth->createPermission('deleteProduct');
        $auth->add($deleteProductPermission);
        $updateProductPermission = $auth->createPermission('updateProduct');
        $auth->add($updateProductPermission);
        $createProductPermission = $auth->createPermission('createProduct');
        $auth->add($createProductPermission);
        
        $viewCategoriesListPermission = $auth->createPermission('viewCategoriesList');
        $auth->add($viewCategoriesListPermission);
        $viewCategoryPermission = $auth->createPermission('viewCategory');
        $auth->add($viewCategoryPermission);
        $deleteCategoryPermission = $auth->createPermission('deleteCategory');
        $auth->add($deleteCategoryPermission);
        $updateCategoryPermission = $auth->createPermission('updateCategory');
        $auth->add($updateCategoryPermission);
        $createCategoryPermission = $auth->createPermission('createCategory');
        $auth->add($createCategoryPermission);
        
        // Define roles with permissions
        
        $moderatorRole = $auth->createRole('moderator');
        $auth->add($moderatorRole);
        $auth->addChild($moderatorRole, $viewOrdersListPermission);
        $auth->addChild($moderatorRole, $viewOrderPermission);
        $auth->addChild($moderatorRole, $deleteOrderPermission);
        $auth->addChild($moderatorRole, $updateOrderPermission);
        
        $auth->addChild($moderatorRole, $viewUsersListPermission);
        $auth->addChild($moderatorRole, $viewUserPermission);
        
        $auth->addChild($moderatorRole, $viewProductsListPermission);
        $auth->addChild($moderatorRole, $viewProductPermission);
        $auth->addChild($moderatorRole, $createProductPermission);
        
        $auth->addChild($moderatorRole, $viewCategoriesListPermission);
        $auth->addChild($moderatorRole, $viewCategoryPermission);
        $auth->addChild($moderatorRole, $updateCategoryPermission);
        $auth->addChild($moderatorRole, $createCategoryPermission);
        
        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);
        $auth->addChild($adminRole, $moderatorRole);
        $auth->addChild($adminRole, $deleteUserPermission);
        $auth->addChild($adminRole, $updateUserPermission);
        
        $auth->addChild($adminRole, $updateProductPermission);
        $auth->addChild($adminRole, $deleteProductPermission);
        
        $auth->addChild($adminRole, $deleteCategoryPermission);
        
        // Create admin user
        // Расчитывается, что после создания суперпользователя пароль будет изменен (или права админа переданы другому пользователю).     
        $user = new User([
            'email' => 'admin@admin.com',
            'username' => 'Admin',
            'status' => 10,
            'password_hash' => '$2y$13$SnkJtRkKuXyYMsG/WOG.puSmNsWLTQWfNcSi/ke1Q7q9Ne9Ogo8/G', // qwe
        ]);
        $user->generateAuthKey();
        $user->save();
        // Assign admin role to 
        $auth->assign($adminRole, $user->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        echo "m210316_134705_create_rbac_data cannot be reverted.\n";

        return false;
    }

}
