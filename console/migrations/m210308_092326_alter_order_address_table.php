<?php

use yii\db\Migration;

/**
 * Class m210308_092326_alter_order_address_table
 */
class m210308_092326_alter_order_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%orders}}', 'address', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%orders}}', 'address');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210308_092326_alter_order_address_table cannot be reverted.\n";

        return false;
    }
    */
}
