<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cartdb}}`.
 */
class m210305_171707_create_cartdb_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cartdb}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'product_id' => $this->integer(),
            'image' => $this->string(),
            'sum' => $this->decimal(),
            'qty' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cartdb}}');
    }
}
