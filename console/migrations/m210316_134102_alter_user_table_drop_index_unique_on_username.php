<?php

use yii\db\Migration;

/**
 * Class m210316_134102_alter_user_table_drop_index_unique_on_username
 */
class m210316_134102_alter_user_table_drop_index_unique_on_username extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('username', '{{%user}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('username' ,'{{%user}}', 'username', $unique = true);
    }
}
