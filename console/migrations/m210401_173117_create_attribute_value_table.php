<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attribute_value}}`.
 */
class m210401_173117_create_attribute_value_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attribute_value}}', [
            'id' => $this->primaryKey(),
            'attr_group_id' => $this->integer(),
            'value' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%attribute_value}}');
    }
}
