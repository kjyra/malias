<?php

use yii\db\Migration;

/**
 * Class m210309_150949_alter_product_bestseller_table
 */
class m210309_150949_alter_product_bestseller_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'bestseller', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'bestseller');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210309_150949_alter_product_bestseller_table cannot be reverted.\n";

        return false;
    }
    */
}
