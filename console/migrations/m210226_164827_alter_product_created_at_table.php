<?php

use yii\db\Migration;

/**
 * Class m210226_164827_alter_product_created_at_table
 */
class m210226_164827_alter_product_created_at_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%product}}', 'created_at', $this->integer());
        $this->addColumn('{{%product}}', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%product}}', 'created_at');
        $this->dropColumn('{{%product}}', 'updated_at');
    }
}
