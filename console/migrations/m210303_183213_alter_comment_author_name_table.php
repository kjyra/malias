<?php

use yii\db\Migration;

/**
 * Class m210303_183213_alter_comment_author_name_table
 */
class m210303_183213_alter_comment_author_name_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%comment}}', 'author_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%comment}}', 'author_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210303_183213_alter_comment_author_name_table cannot be reverted.\n";

        return false;
    }
    */
}
