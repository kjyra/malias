<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sort}}`.
 */
class m210408_170256_create_sort_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sort}}', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
            'title' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sort}}');
    }
}
