<?php

use yii\db\Migration;

/**
 * Class m210408_173317_alter_sort_table_value_column
 */
class m210408_173317_alter_sort_table_value_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("{{%sort}}", 'value', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("{{%sort}}", 'value');
    }
    
}
