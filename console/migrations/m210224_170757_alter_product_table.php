<?php

use yii\db\Migration;

/**
 * Class m210224_170757_alter_product_table
 */
class m210224_170757_alter_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('{{%product}}', 'secondary_img', $this->string());
        $this->addColumn('{{%product}}', 'slider', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropColumn('{{%product}}', 'secondary_img');
        $this->dropColumn('{{%product}}', 'slider');
    }

}
