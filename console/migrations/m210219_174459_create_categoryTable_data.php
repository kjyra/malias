<?php

use yii\db\Migration;

/**
 * Class m210219_174459_create_categoryTable_data
 */
class m210219_174459_create_categoryTable_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%category}}', [
            'id' => 1,
            'title' => 'Cameras & Photography',
            'description' => 'Category about cameras and photos has a children categories',
            'status' => 1
        ]);
        $this->insert('{{%category}}', [
            'id' => 2,
            'title' => 'Tv & Audio',
            'description' => '',
            'status' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210219_174459_create_categoryTable_data cannot be reverted.\n";

        return false;
    }
    */
}
