<?php

use yii\db\Migration;

/**
 * Class m210303_192139_alter_comment_product_id_table
 */
class m210303_192139_alter_comment_product_id_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%comment}}', 'product_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%comment}}', 'product_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210303_192139_alter_comment_product_id_table cannot be reverted.\n";

        return false;
    }
    */
}
