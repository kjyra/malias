<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attribute_group}}`.
 */
class m210401_173050_create_attribute_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attribute_group}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%attribute_group}}');
    }
}
