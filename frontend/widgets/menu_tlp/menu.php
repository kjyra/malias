<?php 
use yii\helpers\Url;
?>
<?php if($i > 7): ?>

    <!-- MENU ACCORDION START -->
    <?php if(isset($category['children'])): ?>
        <!-- Single menu start -->
        <li class="arrow-plus rx-child">
            <a href="<?php echo Url::to(['/category/default/view', 'id' => $category['id']]); ?>"><?php echo $category['title']; ?></a>
            <!--  MEGA MENU START -->
            <div class="cat-left-drop-menu">
            <?php foreach($category['children'] as $children): ?>
                <div class="cat-left-drop-menu-left">
                    <a class="menu-item-heading" href="<?php echo Url::to(['/category/default/view', 'id' => $children['id']]); ?>">
                        <?php echo $children['title']; ?>
                    </a>
                    <?php if($children['children']): ?>
                    <ul>
                        <?php foreach($children['children'] as $child): ?>
        <!--                <li>
                            <a href="<?php // echo Url::to(['/category/default/view', 'id' => $child['id']]); ?>"><?php // echo $child['title']; ?></a>
                        </li>-->
                        <?php echo $this->catToTemplate($child); ?>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
            </div>
            <!-- MEGA MENU END -->
        </li>
        <!-- Single menu end -->
        <?php else: ?>
        <li class=" rx-child">
            <a href="<?php echo Url::to(['/category/default/view', 'id' => $category['id']]); ?>"><?php echo $category['title']; ?></a>
        </li>
        <?php endif; ?>
    <?php if($i == $countOfCategories): ?>
    <li class=" rx-parent">
        <a class="rx-default">
            More categories <span class="cat-thumb  fa fa-plus"></span> 
        </a>
        <a class="rx-show">
            close menu <span class="cat-thumb  fa fa-minus"></span>
        </a>
    </li>
    <?php endif; ?>
    <!-- MENU ACCORDION END -->
    
<?php else: ?>
    <?php if(isset($category['children'])): ?>
    <!-- Single menu start -->
    <li class="arrow-plus">
        <a href="<?php echo Url::to(['/category/default/view', 'id' => $category['id']]); ?>"><?php echo $category['title']; ?></a>
        <!--  MEGA MENU START -->
        <div class="cat-left-drop-menu">
        <?php foreach($category['children'] as $children): ?>
            <div class="cat-left-drop-menu-left">
                <a class="menu-item-heading" href="<?php echo Url::to(['/category/default/view', 'id' => $children['id']]); ?>"><?php echo $children['title']; ?></a>
                <?php if($children['children']): ?>
                <ul>
                    <?php foreach($children['children'] as $child): ?>
    <!--                <li>
                        <a href="<?php // echo Url::to(['/category/default/view', 'id' => $child['id']]); ?>"><?php // echo $child['title']; ?></a>
                    </li>-->
                        <?php echo $this->catToTemplate($child); ?>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        </div>
        <!-- MEGA MENU END -->
    </li>
    <!-- Single menu end -->
    <?php else: ?>
    <li>
        <a href="<?php echo Url::to(['/category/default/view', 'id' => $category['id']]); ?>"><?php echo $category['title']; ?></a>
    </li>
    <?php endif; ?>
<?php endif; ?>