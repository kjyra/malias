<?php 
use yii\helpers\Url;
?>
<div class="toch-table">
    <table class="table table-striped table-bordered">
        <tbody>
            <tr>
                <td><strong><?php echo $c->author_name; ?></strong></td>
                <td class="text-right"><strong><?php echo Yii::$app->formatter->asDate($c->created_at); ?></strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <p><?php echo $c->text; ?></p>
                    <span><i class="fa fa-star"></i></span>
                    <span><i class="fa fa-star"></i></span>
                    <span><i class="fa fa-star"></i></span>
                    <span><i class="fa fa-star"></i></span>
                    <span><i class="fa fa-star-o"></i></span>
                </td>
            </tr>
        </tbody>
    </table>
    <?php if($this->user && $this->user->id == $c->user_id): ?>
        <p><a href="<?php echo Url::to(['/comment/default/delete', 'id' => $c->id]); ?>">Удалить комментарий</a></p>
    <?php endif; ?>
</div>