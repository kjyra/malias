<?php

namespace frontend\widgets\comment;

use Yii;
use yii\base\Widget;
use frontend\modules\product\models\Product;

/**
 * Description of CommentWidget
 *
 * @author grigo
 */
class CommentWidget extends Widget
{
    
    public Product $product;
    public $user;
    public $tpl;
    public $menuHtml;
    
    public function init()
    {
        if($this->tpl === null) {
            $this->tpl = 'comments';
        }
        $this->tpl .= '.php';
    }
    
    public function run()
    {
        $comments = $this->product->getComments();
        $this->user = Yii::$app->user->identity;
        
        $this->menuHtml = $this->getCommentHtml($comments);
        return $this->menuHtml;
    }
    
    public function getCommentHtml($comments)
    {
        $str = '';
        foreach ($comments as $c) {
            $str .= $this->catToTemplate($c);
        }
        return $str;
    }
    
    public function catToTemplate($c)
    {
        ob_start();
        include __DIR__ . '/category_tpl/' . $this->tpl;
        return ob_get_clean();
    }
}
