<div class="sort">
    <label>Sort By:</label>
    <select id="sort">
        <?php foreach($this->data as $sort): ?>
            <?php
            if(!empty($get) && $get == $sort['number']) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            ?>
                <option <?php echo $selected; ?> value="<?php echo $sort['number']; ?>"><?php echo $sort['title']; ?></option>
        <?php endforeach; ?>
    </select>
</div>