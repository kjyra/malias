<?php

namespace frontend\widgets\sort;

use Yii;
use yii\base\Widget;
use frontend\modules\category\models\sort\Sort;

/**
 * Description of SortWidget
 *
 * @author grigo
 */
class SortWidget extends Widget
{
    
    public $tpl;
    public $data;
    public $html;
    
    public function init()
    {
        if($this->tpl === null) {
            $this->tpl = __DIR__ . '/sort_tpl';
        }
        $this->tpl .= '.php';
    }    
    
    public function run()
    {
        $this->data = Yii::$app->cache->get('data');
        
        if(!$this->data) {
            $this->data = self::getSort();
            Yii::$app->cache->set('data', $this->data, 5);
        }
        
        $this->html = $this->getHtml();
        
        
        return $this->html;
    }
    
    private static function getSort()
    {
        return Sort::find()->asArray()->all();
    }
    
    private function getHtml()
    {
        ob_start();
        $get = self::getUrlSort();
        require $this->tpl;
        return ob_get_clean();
    }
    
    public static function getUrlSort()
    {
        $sort = null;
        if(Yii::$app->request->get('sort')) {
            $sort = Yii::$app->request->get('sort');
        }
        return $sort;
    }   
    
}
