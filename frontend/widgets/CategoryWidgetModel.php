<?php

namespace frontend\widgets;

use frontend\modules\category\models\Category;

/**
 * Description of CategoryModel
 *
 * @author grigo
 */
class CategoryWidgetModel implements interfaces\CategoryWidgetInterface
{
    
    public function getCountOfRootCategories()
    {
        return (int) Category::find()->where('parent_id = 0')->count();
    }
    
    public function findAll()
    {
        return Category::find()->select('id, parent_id, title')->indexBy('id')->asArray()->all();
    }

}
