<?php

namespace frontend\widgets\filter;

use Yii;
use yii\base\Widget;

/**
 * Description of FilterWidget'
 *
 * @author grigo
 */
class FilterWidget extends Widget
{
    
    public $tpl;
    public $attrs;
    public $groups;
    
    public function __construct() {
        $this->tpl = __DIR__ . '/filter_tpl.php';
    }
    
    public function run()
    {
        $this->groups = Yii::$app->cache->get('group_filter');
        if(!$this->groups) {
            $this->groups = $this->getGroups();
            Yii::$app->cache->set('group_filter', $this->groups, 1);
        }
        $this->attrs = Yii::$app->cache->get('attrs_filter');
        if(!$this->attrs) {
            $this->attrs = self::getAttrs();
            Yii::$app->cache->set('attrs_filter', $this->attrs, 1);
        }
        $filters = $this->getHtml();
        return $filters;
    }
    
    public function getGroups()
    {
        return \frontend\modules\product\models\filter\AttributeGroup::find()->asArray()->all();
    }
    
    public static function getAttrs()
    {
        $data = \frontend\modules\product\models\filter\AttributeValue::find()->all();
        
        $attrs = [];
        foreach($data as $attr_v) {
            $attrs[$attr_v['attr_group_id']][$attr_v['id']] = $attr_v['value'];
        }
        
        return $attrs;
    }
    
    public function getHtml()
    {
        ob_start();
        $filter = self::getFilter();
        if(!empty($filter)) {
            $filter = explode(',', $filter);
        }
        require $this->tpl;
        return ob_get_clean();
    }
    
    public static function getFilter()
    {
        $filter = null;
        if(!empty(Yii::$app->request->get('filter'))) {
            $filter = preg_replace("#[^\d,]+#", '', Yii::$app->request->get('filter'));
            $filter = trim($filter, ',');
        }
        return $filter;
    }
    
    public static function getCountGroups($filter)
    {
        $filter = explode(',', $filter);
        $attrs = Yii::$app->cache->get('attrs_filter');
        if(!$attrs) {
            $attrs = self::getAttrs();
        }
        
        $groupIds = [];
        foreach($attrs as $key => $item) {
            foreach($item as $k => $v) {
                if(in_array($k, $filter)) {
                    $groupIds[] = $key;
                    break;
                }
            }
        }
        return count($groupIds);
    }
    
}
