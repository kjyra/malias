<?php
//debug($this->groups);
//debug($this->attrs);
?>
<div class="shop-filter">
    <div class="area-title">
        <h3 class="title-group gfont-1">Filters Products</h3>
    </div>
    <h4 class="shop-price-title">Price</h4>
    <div class="info_widget">
        <div class="price_filter">
            <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 3.57143%; width: 91.0714%;">
                    
                </div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 3.57143%;">
                    
                </span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 94.6429%;">
                    
                </span>
            </div>
            <div class="price_slider_amount">
                <input type="text" id="amount" name="price" placeholder="Add Your Price">
                <input type="submit" value="Filter">  
            </div>
        </div>
    </div>
</div>
<?php if($this->groups): ?>
<div class="accordion_one">
    <?php $i = 0; foreach($this->groups as $group): ?>
    <h4><a class="accordion-trigger" data-toggle="collapse" href="#div<?php echo $i; ?>"><?php echo $group['title']; ?></a></h4>
    <div id="div<?php echo $i; ?>" class="collapse in">
        <?php if($this->attrs): ?>
        <div class="filter-menu">
            <ul>
                <?php foreach($this->attrs[$group['id']] as $attr_id => $attr_value): ?>
                <?php 
                    if(!empty($filter) && in_array($attr_id, $filter)) {
                        $checked = 'checked';
                    } else {
                        $checked = null;
                    }
                ?>
                    <li>
                        <a><input type="checkbox" value="<?php echo $attr_id; ?>" <?php echo $checked; ?>>&nbsp;&nbsp;&nbsp;<?php echo $attr_value; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>
    </div>
    <?php $i++; endforeach; ?>
</div>
<?php endif; ?>
