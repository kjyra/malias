<?php

namespace frontend\widgets\bestseller;

use Yii;
use yii\bootstrap\Widget;
use frontend\modules\product\models\ProductFindModel;

/**
 * Description of BestsellerWidget
 *
 * @author grigo
 */
class BestsellerWidget extends Widget
{
    
    public $tpl;
    public $data;
    public $menuHtml;
    
    const DEFAULT_COUNT = 8;
    const DEFAULT_COUNT_PRODUCTS_ON_PAGE = 4;
    
    public function init()
    {
        if($this->tpl === null) {
            $this->tpl = 'bestseller';
        }
        $this->tpl .= '.php';
    }
    
    public function run()
    {
        $menu = Yii::$app->cache->get('bestseller_menu');
        if($menu) {
            return $menu;
        }
        $this->data = ProductFindModel::findBestsellerProducts(self::DEFAULT_COUNT);
        $this->menuHtml = $this->getMenuHtml($this->data);
        Yii::$app->cache->set('bestseller_menu', $this->menuHtml, 100);
        return $this->menuHtml;
    }
    
    public function getMenuHtml($data)
    {    
        $i = 0;
        $str = '<div class="small-product-area carosel-navigation">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group gfont-1">Bestseller</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-bestseller sidebar">';
        $count = count($data);
        while($i < $count) {
            $j = 0;
            $str .= '<div class="col-xs-12">';
            while($j < self::DEFAULT_COUNT_PRODUCTS_ON_PAGE) {
                if($i == $count) {
                    break;
                }
                $str .= $this->catToTemplate($data[$i]);
                $i++;
                $j++;
            }
            $str .= '</div>';
        }
        $str .= '</div>
            </div>
        </div>';
        return $str;
    }
    
    public function catToTemplate($product)
    {
        ob_start();
        include __DIR__ . '/bestseller_tpl/' . $this->tpl;
        return ob_get_clean();
    }
    
}
