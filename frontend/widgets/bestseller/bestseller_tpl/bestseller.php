<?php
use yii\helpers\Url;
?>
    <!-- Start Single-Product -->
    <div class="single-product">
        <div class="product-img">
            <a href="<?php echo Url::to(['/product/default/view', 'id' => $product->id]); ?>">
                <img class="primary-img" src="<?php echo $product->getImage(); ?>" width="100px" alt="Product">
            </a>
        </div>
        <div class="product-description">
            <h5><a href="<?php echo Url::to(['/product/default/view', 'id' => $product->id]); ?>"><?php echo $product->title; ?></a></h5>
            <div class="price-box">
                <span class="price">$<?php echo $product->price; ?></span>
                <?php if($product->old_price > $product->price): ?>
                <span class="old-price">$<?php echo $product->old_price; ?></span>
                <?php endif; ?>
            </div>
            <span class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-o"></i>
            </span>
        </div>
    </div>
    <!-- End Single-Product -->

