<?php

namespace frontend\widgets;

use Yii;
use yii\bootstrap\Widget;
use frontend\widgets\CategoryWidgetModel;

/**
 * Description of CategoryWidget
 *
 * @author grigo
 */
class CategoryWidget extends Widget
{
    
    public $tpl;
    public $ul_class;
    public $data;
    public $tree;
    public $menuHtml;
    public $categoryModel;

    public function init()
    {
        $this->categoryModel = new CategoryWidgetModel();
        parent::init();
        if ($this->ul_class === null) {
            $this->ul_class = 'menu';
        }
        if ($this->tpl === null) {
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';
    }

    public function run()
    {
        $menu = Yii::$app->cache->get('menu');
        if ($menu) {
            return $menu;
        }
        $this->data = $this->categoryModel->findAll();
        $this->tree = $this->getTree();
        $this->menuHtml = '<ul class="' . $this->ul_class . '">';
        $this->menuHtml .= $this->getMenuHtml($this->tree);
        $this->menuHtml .= '</ul>';
        Yii::$app->cache->set('menu', $this->menuHtml, 100);
        return $this->menuHtml;
    }
    
    protected function getTree()
    {
        $tree = [];
        foreach ($this->data as $id => &$node) {
            if (!$node['parent_id']) {
                $tree[$id] = &$node;
            } else {
                $this->data[$node['parent_id']]['children'][$node['id']] = &$node;
            }
        }
        return $tree;
    }

    protected function getMenuHtml($tree)
    {
        $str = '';
        $i = 0;
        $countOfCategories = $this->categoryModel->getCountOfRootCategories();
        foreach ($tree as $category) {
            $i++;
            $str .= $this->catToTemplate($category, $i, $countOfCategories);
        }
        // $str = $this->moreCategoriesFunction($str);
        return $str;
    }

    protected function catToTemplate($category, $i = 0, $countOfCategories = 0)
    {
        ob_start();
        include __DIR__ . '/menu_tlp/' . $this->tpl;
        return ob_get_clean();
    }

}
