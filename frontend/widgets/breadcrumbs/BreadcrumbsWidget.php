<?php

namespace frontend\widgets\breadcrumbs;

use yii\base\Widget;
use frontend\modules\category\models\Category;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Description of BreadcrumbsWidget
 *
 * @author grigo
 */
class BreadcrumbsWidget extends Widget
{
    
    public $cats;
    public $category_id;
    public $title;
    public $html;
    
    public function run()
    {
        $cats = Category::find()->select(['id', 'title', 'parent_id'])->asArray()->all();
        $this->cats = ArrayHelper::index($cats, 'id');
//        debug($this->cats, 1);
        $this->html = $this->getHtml();
        return $this->html;
    }
    
    public function getHtml()
    {
        $breadcrumbs_array = $this->getBreadcrumbsRoute($this->cats, $this->category_id);
        $count = count($breadcrumbs_array);
        
        $breadcrumbs = '<div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="/">Home</a></li>';
        $i = 1;
        foreach($breadcrumbs_array as $k => $bread) {
            if($i == $count && !$this->title) {
                $breadcrumbs .= "<li class='active'><a>" . $bread . "</a></li>";
            } else {
                $breadcrumbs .= "<li><a href='" . Url::to(['/category/default/view', 'id' => $k]) . "'>" . $bread . "</a></li>";                
            }
            $i++;
        }
//        var_dump($breadcrumbs);
        if($this->title) {
            $breadcrumbs .= '<li class="active"><a>' . $this->title . '</a></li>';
        }
        
        $breadcrumbs .= '</ul>
            </div>
        </div>';
        
        return $breadcrumbs;
    }
    
    public function getBreadcrumbsRoute($cats, $cat_id)
    {
        if(!$cat_id) return false;
        $breadcrumbs = [];
        foreach($cats as $k => $cat) {
            if(isset($cats[$cat_id])) {
                $breadcrumbs[$cats[$cat_id]['id']] = $cats[$cat_id]['title'];
                $cat_id = $cats[$cat_id]['parent_id'];
            } else break;
        }
        return array_reverse($breadcrumbs, true);
    }
    
}
