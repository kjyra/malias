<?php

namespace frontend\widgets\interfaces;

/**
 *
 * @author grigo
 */
interface CategoryWidgetInterface
{
    public function getCountOfRootCategories();
    public function findAll();
}
