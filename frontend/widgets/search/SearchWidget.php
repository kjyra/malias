<?php

namespace frontend\widgets\search;

use Yii;
use frontend\widgets\search\forms\SearchForm;
use frontend\modules\category\models\CategoryFindModel;

/**
 * Description of SearchWidget
 *
 * @author grigo
 */
class SearchWidget extends \yii\base\Widget
{

    public function run()
    {
        $model = new SearchForm();
        $categories = $this->getCategories(3);

        $model->load(Yii::$app->request->post());

        return $this->render('search', [
            'model' => $model,
            'categories' => $categories,
        ]);
    }
    
    public function getCategories($limit = 3)
    {
        $categories = []; 
        $rootCategories = CategoryFindModel::findRootCategories($limit);
        foreach($rootCategories as $category) {
            if($category->hasChildren()) {

                foreach($category->children as $child) {
                    
                    $cats[$category->title] = [
                        $child->id => $child->title,
                        $child->title => $child->id,
                    ];
                    $categories[0] = 'All Categories';
                    $categories[$category->title][$cats[$category->title][$child->title]] = $cats[$category->title][$child->id];
                    
                }
                
            }
        }
        return $categories;
    }
} 
