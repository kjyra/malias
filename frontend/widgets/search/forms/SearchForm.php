<?php

namespace frontend\widgets\search\forms;

use yii\base\Model;
use frontend\models\ProductSearch;

/**
 * Description of SearchForm
 *
 * @author grigo
 */
class SearchForm extends Model
{
    
    public $keyword;
    public $category_id;
    
    public function rules()
    {
        return [
            ['category_id', 'required'],
            ['category_id', 'integer'],
            
            ['keyword', 'required'],
            ['keyword', 'string', 'min' => 3],
            ['keyword', 'trim'],
        ];
    }
    
    public function search()
    {
        if($this->validate()) {
            $search = new ProductSearch();
            $result = $search->fulltextSearchActiveRecord($this->keyword, $this->category_id);
            return $result;          
        }
        return false;
    }
}
