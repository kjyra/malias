<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="search-by-category">
    <?php $form = ActiveForm::begin([
            'id' => 'contact-form',
            'action' => '/product/search',
                    'fieldConfig' => [
                        'options' => [
                            'tag' => 'div',
                        ],
                    ],
        ]);
        ?>
    <div class="search-container">
        <?php echo $form->field($model, 'category_id', ['template' => "{label}\n{input}"])->dropDownList($categories, [
            'options' => [
                '0' => ['Selected' => true]
            ]
        ])->label(false); ?>
    </div>
    <div class="header-search">
        <?php echo $form->field($model, 'keyword', ['template' => "{label}\n{input}"])->textInput(['placeholder' => 'Search', 'value' => $model->keyword])->label(false); ?>
        <?php echo Html::submitButton("<i class='fa fa-search'></i>"); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>