<?php
return [
    'adminEmail' => 'admin@example.com',
    'defaultTextSizeLimit' => 30,
    'languages' => ['ru-RU', 'en-US'],
];
