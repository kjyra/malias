<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        [
            'class' => 'frontend\components\LanguageSelector'
        ]
    
    ],
    'language' => 'ru-RU',
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
        'category' => [
            'class' => 'frontend\modules\category\Module',
        ],
        'product' => [
            'class' => 'frontend\modules\product\Module',
        ],
        'comment' => [
            'class' => 'frontend\modules\comment\Module',
        ],
        'cart' => [
            'class' => 'frontend\modules\cart\Module',
        ],
        'order' => [
            'class' => 'frontend\modules\order\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl'=>['user/default/login'], 
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '/' => '/site/index',
                'category/<id:\d+>/page/<page:\d+>' => 'category/default/view',
                'category/<id:\d+>' => 'category/default/view',
                'product/search' => 'product/default/search',
                'product/<id:\d+>' => 'product/default/view',
                'cart/checkout' => 'cart/default/checkout',
                'login' => 'user/default/login',
                'signup' => 'user/default/signup',
                'cart' => 'cart/default/view',
            ],
        ],
        'stringHelper' => [
            'class' => 'frontend\components\StringHelper',
        ],
        'emailService' => [
            'class' => 'frontend\components\EmailService',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ]
            ]
        ],
    ],
    'params' => $params,
];
