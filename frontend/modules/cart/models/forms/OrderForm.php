<?php

namespace frontend\modules\cart\models\forms;

use Yii;
use yii\base\Model;
use frontend\models\Order;
use frontend\modules\product\models\Product;
use frontend\models\events\OrderCreatedEvent;
use frontend\models\events\ProductBoughtEvent;
use frontend\models\OrderProduct;

/**
 * Description of OrderForm
 *
 * @author grigo
 */
class OrderForm extends Model
{
    public $user_id;
    public $name;
    public $address;
    public $number;
    public $email;
    public $text;
    public $qty;
    public $total;
    
    const ORDER_CREATED_EVENT = 'order_created';
    const PRODUCTS_BOUGHT_EVENT = 'products_bought';
    
    public function __construct()
    {
        $this->on(self::ORDER_CREATED_EVENT, [Yii::$app->emailService, 'notifyUser']);
        $this->on(self::ORDER_CREATED_EVENT, [Yii::$app->emailService, 'notifyAdmin']);
        $this->on(self::PRODUCTS_BOUGHT_EVENT, [Product::class, 'updateProductBestsellerList']);
    }
    
    public function rules()
    {
        return [
            [['name', 'address', 'email', 'number'], 'required'],
            [['name', 'address', 'email', 'text'], 'string'],
            ['email', 'email'],
            ['number', 'number'],
        ];
    }
    
    public function save($products)
    {
        if($this->validate()) {
            $order = new Order();
            $order_product = new OrderProduct();
            $order->user_id = $this->user_id;
            $order->name = $this->name;
            $order->qty = $this->qty;
            $order->total = $this->total;
            $order->number = $this->number;
            $order->text = $this->text;
            $order->address = $this->address;
            $order->email = $this->email;
            $order->status = Order::NEW_ORDER_STATUS;
            $transaction = Yii::$app->getDb()->beginTransaction();
            if(!$order->save() || !$order_product->saveOrderProducts($products, $order->id)) {
                $transaction->rollBack();
                return false;
            } else {
                $event = new OrderCreatedEvent($this->email, 'Новый заказ принят!');
                $this->trigger(self::ORDER_CREATED_EVENT, $event);
                $productEvent = new ProductBoughtEvent($products);
                $this->trigger(self::PRODUCTS_BOUGHT_EVENT, $productEvent);
                $transaction->commit();
                return true;
            }
        }
    }
}
