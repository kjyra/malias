<?php

namespace frontend\modules\cart\models;

use Yii;
use yii\base\Model;
use frontend\models\User;
use frontend\models\Cartdb;
use frontend\modules\cart\models\interfaces\FindCart;

/**
 * Description of CartdbFind
 *
 * @author grigo
 */
class CartdbFindModel extends Model implements FindCart
{
    public static function findCart(User $user)
    {
        return Cartdb::find()->where(['user_id' => $user->id])->all();
    }
    
    public static function findProductInCartById($id)
    {
        return Cartdb::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['product_id' => $id])->one();
    }
}
