<?php

namespace frontend\modules\cart\models;

use yii\base\Model;

/**
 * Description of CartSession
 *
 * @author grigo
 */
class CartSession extends Model
{
    
    public function addToCart($product, $qty = 1)
    {
            if(isset($_SESSION['cart'][$product->id])) {
                $_SESSION['cart'][$product->id]['qty'] += $qty;
            } else {
                $_SESSION['cart'][$product->id] = [
                    'product_id' => $product->id,
                    'title' => $product->title,
                    'image' => $product->getImage(),
                    'price' => $product->price,
                    'qty' => $qty
                ];
            }
            $_SESSION['qty'] = isset($_SESSION['qty']) ? $_SESSION['qty'] + $qty : $qty;
            $_SESSION['sum'] = isset($_SESSION['sum']) ? $_SESSION['sum'] + $qty * $product->price : $qty * $product->price;
    }
    
    public function update($id, $qty)
    {
        if(!isset($_SESSION['cart'][$id])) {
            return false;
        }
        if($qty <= 0) {
            $this->delete($id);
        } else {
            $_SESSION['cart'][$id]['qty'] = $qty;
            $quantity;
            $sum;
            foreach($_SESSION['cart'] as $cartItem) {
                $quantity += $cartItem['qty'];
                $sum += $cartItem['price'] * $cartItem['qty'];
            }
            $_SESSION['qty'] = $quantity;
            $_SESSION['sum'] = $sum;
        }
    }    
    
    public function delete($id)
    {
        $_SESSION['qty'] -= $_SESSION['cart'][$id]['qty'];
        $_SESSION['sum'] -= $_SESSION['cart'][$id]['price'] * $_SESSION['cart'][$id]['qty'];
        unset($_SESSION['cart'][$id]);
    }
    
    public function clear($session)
    {
        $session->remove('cart');
        $session->remove('sum');
        $session->remove('qty');
    }
    
    public static function getProductList()
    {
        $products = [];
        foreach($_SESSION['cart'] as $product) {
            $products[] = $product;
        }
    }
    
}
