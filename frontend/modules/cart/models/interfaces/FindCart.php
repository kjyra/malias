<?php

namespace frontend\modules\cart\models\interfaces;

use frontend\models\User;

/**
 *
 * @author grigo
 */
interface FindCart
{
    public static function findCart(User $user);
    public static function findProductInCartById($id);
}
