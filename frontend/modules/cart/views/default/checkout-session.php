<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="<?php echo Url::to(['/']); ?>">Home</a></li>
                    <li><a href="<?php echo Url::to(['/cart/default/view']); ?>">Shopping Cart</a></li>
                    <li class="active"><a href="#">Checkout</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
                <?php echo $this->render('//layouts/inc/sidebar'); ?>
                <!-- END CATEGORY-MENU-LIST -->
                <!-- START SMALL-PRODUCT-AREA -->
                <div class="small-product-area carosel-navigation hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="area-title">
                                <h3 class="title-group gfont-1">Bestseller</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="active-bestseller sidebar">
                            <div class="col-xs-12">
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Various Versions</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/2.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/3.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/4.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                            </div>
                            <div class="col-xs-12">
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/5.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Various Versions</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/6.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/7.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/8.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SMALL-PRODUCT-AREA -->
            </div>
            <div class="col-md-9">
                <!-- START PRODUCT-BANNER -->
                <div class="product-banner">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="banner">
                                <a href="#"><img src="img/banner/12.jpg" alt="Product Banner"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-BANNER -->
                <!-- Start checkout-area -->
                <?php if($_SESSION['cart']): ?>
                <div class="checkout-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cart-title">
                                <h2 class="entry-title">Checkout</h2>
                            </div>
                            <!-- Accordion start -->
                            <?php $form = ActiveForm::begin(); ?>
                            <div class="panel-group" id="accordion">
                            
                                <!-- Start 1 Checkout-options -->
                                <div class="panel panel_default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-trigger" data-toggle="collapse" data-parent="#accordion" href="#checkout-options">Step 1: Checkout Options  <i class="fa fa-caret-down"></i> </a>
                                        </h4>
                                    </div>
                                    <div id="checkout-options" class="collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6 col-xs-12">
                                                    <div class="checkout-collapse">
                                                        <h3 class="title-group-3 gfont-1">New Customer</h3>
                                                        <p>Checkout Options</p>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="register" name="account" checked/>
                                                                Register Account
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" value="guest" name="account"/>
                                                                Guest Checkout
                                                            </label>
                                                        </div>
                                                        <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                                                        <input type="submit" class="btn btn-primary" value="Continue"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xs-12">
                                                    <div class="checkout-collapse">
                                                        <h3 class="title-group-3 gfont-1">Returning Customer</h3>
                                                        <p>I am a returning customer</p>
                                                        <div class="form-group">
                                                            <label>E-mail</label>
                                                            <input type="email" class="form-control" name="email" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input type="password" class="form-control" />
                                                            <a href="#">Forgotten Password</a>
                                                        </div>
                                                        <input type="submit" class="btn btn-primary" value="Login"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Checkout-options -->
                                <!-- Start 3 shipping-Address -->
                                <div class="panel panel_default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-trigger collapsed" data-toggle="collapse" data-parent="#accordion" href="#shipping-address">Step 2: Delivery Details <i class="fa fa-caret-down"></i> </a>
                                        </h4>
                                    </div>
                                    <div id="shipping-address" class="collapse">
                                        <div class="panel-body">
                                            <div class="form-horizontal">
                                                
                                                <div class="form-group">
                                           
                                                    <?php echo $form->field($model, 'name', [
                                                        'template' => "{label}\n<div class='col-sm-10'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-2 control-label' ]
                                                    ])->textInput(['class' => 'form-control']); ?>
                   
                                                </div>
                                                <div class="form-group">
                                                    
                                                    <?php echo $form->field($model, 'email', [
                                                        'template' => "{label}\n<div class='col-sm-10'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-2 control-label' ]
                                                    ])->textInput(['class' => 'form-control']); ?>
                                            
                                                </div>
                                                <div class="form-group">
                                     
                                                    <?php echo $form->field($model, 'number', [
                                                        'template' => "{label}\n<div class='col-sm-10'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-2 control-label' ]
                                                    ])->textInput(['class' => 'form-control']); ?>
      
                                                </div>
                                                <div class="form-group">
                                          
                                                    <?php echo $form->field($model, 'address', [
                                                        'template' => "{label}\n<div class='col-sm-10'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-2 control-label' ]
                                                    ])->textInput(['class' => 'form-control']); ?>
                                    
                                                </div>
                                                
                                                <div class="form-group">
                                          
                                                    <?php echo $form->field($model, 'text', [
                                                        'template' => "{label}\n<div class='col-sm-10'>{input}</div>\n{hint}\n{error}",
                                                        'labelOptions' => [ 'class' => 'col-sm-2 control-label' ]
                                                    ])->textInput(['class' => 'form-control']); ?>
                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End shipping-Address -->
                                <!-- Start 6 Checkout-Confirm -->
                                <div class="panel panel_default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-trigger collapsed" data-toggle="collapse" data-parent="#accordion" href="#checkout-confirm">Step 3: Confirm Order <i class="fa fa-caret-down"></i> </a>
                                        </h4>
                                    </div>
                                    <div id="checkout-confirm" class="collapse">
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <td class="text-left">Product Name</td>
                                                            <td class="text-left">Quantity</td>
                                                            <td class="text-left">Unit Price</td>
                                                            <td class="text-left">Total</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach($_SESSION['cart'] as $product): ?>
                                                        <tr>
                                                            <td class="text-left">
                                                                <a href="<?php echo Url::to(['/product/default/view', 'id' => $product['product_id']]); ?>">
                                                                    <?php echo $product['title']; ?></a>
                                                            </td>
                                                            <td class="text-left"><?php echo $product['qty']; ?></td>
                                                            <td class="text-left">$<?php echo $product['price']; ?></td>
                                                            <td class="text-left">$<?php echo $product['qty'] * $product['price']; ?></td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td class="text-right" colspan="4">
                                                                <strong>Total:</strong>
                                                            </td>
                                                            <td class="text-right">$<?php echo $_SESSION['sum']; ?></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="buttons pull-right">
                                                <?php echo Html::submitButton('Confirm Order', ['class' => 'btn btn-primary']); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Checkout-Confirm -->
                            </div>
                            <?php ActiveForm::end(); ?>
                            <!-- Accordion end -->
                        </div>
                    </div>
                </div>
                <?php else: ?>
                <h3>Ваша корзина пуста!</h3>
                <p>
                    <a href="/" class="btn btn-default">
                        Вернуться к покупкам!
                    </a>
                </p>
                <?php endif; ?>
                <!-- End Shopping-Cart -->
            </div>
        </div>
    </div>