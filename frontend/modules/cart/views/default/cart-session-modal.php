<?php 
use yii\helpers\Url;
?>
<a href="<?php echo Url::to(['/cart/default/view']); ?>">
    <span class="cart-icon"><i class="fa fa-shopping-cart"></i></span>
    <span class="cart-total">
        <span class="cart-title">shopping cart</span>
        <span class="cart-item">

            <?php echo $_SESSION['qty'] ?? '0'; ?> item(s)- 

        </span>
        <span class="top-cart-price">
            $<?php echo $_SESSION['sum'] ?? '0'; ?> 
        </span>
    </span>
</a>
<div class="mini-cart-content">
<?php if($_SESSION['cart']): ?>
<?php foreach($_SESSION['cart'] as $id => $product): ?>
    <div class="cart-img-details">
        <div class="cart-img-photo">
            <a href="<?php echo Url::to(['/product/default/view', 'id' => $id]); ?>">
                <img src="<?php echo $product['image']; ?>" alt="#">
            </a>
        </div>
        <div class="cart-img-content">
            <a href="<?php echo Url::to(['/product/default/view', 'id' => $id]); ?>"><h4><?php echo $product['title']; ?></h4></a>
            <span>
                <strong class="text-right">1 x</strong>
                <strong class="cart-price text-right">$<?php echo $product['price']; ?></strong>
            </span>
        </div>
        <div class="pro-del">
            <a href="<?php echo Url::to(['/cart/default/delete', 'id' => $id]); ?>"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="clear"></div>
<?php endforeach; ?>
<div class="cart-inner-bottom">
    <span class="total">
        Total:
        <span class="amount">$<?php echo $_SESSION['sum']; ?></span>
    </span>
    <span class="cart-button-top">
        <a href="<?php echo Url::to(['/cart/default/view']); ?>">View Cart</a>
        <a href="<?php echo Url::to(['/cart/default/checkout']); ?>">Checkout</a>
    </span>
</div>
<?php else: ?>
    <p>Ваша корзина пуста</p>
<?php endif; ?>
</div>