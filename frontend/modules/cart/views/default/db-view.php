<?php

use yii\helpers\Url;
use frontend\models\Cartdb;
?>
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="<?php echo Url::to(['/']); ?>">Home</a></li>
                    <li class="active"><a href="#">Shopping Cart</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
<?php echo $this->render('//layouts/inc/sidebar.php'); ?>	
                <!-- END CATEGORY-MENU-LIST -->
                <!-- START SMALL-PRODUCT-AREA -->
                <div class="small-product-area carosel-navigation  hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="area-title">
                                <h3 class="title-group gfont-1">Bestseller</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="active-bestseller sidebar">
                            <div class="col-xs-12">
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/1.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/2.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Various Versions</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/3.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/4.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                            </div>
                            <div class="col-xs-12">
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/5.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/6.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Various Versions</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/7.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Trid Palm</a></h5>
                                        <div class="price-box">
                                            <span class="price">$99.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                                <!-- Start Single-Product -->
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="#">
                                            <img class="primary-img" src="img/product/small/8.jpg" alt="Product">
                                        </a>
                                    </div>
                                    <div class="product-description">
                                        <h5><a href="#">Established Fact</a></h5>
                                        <div class="price-box">
                                            <span class="price">$90.00</span>
                                            <span class="old-price">$110.00</span>
                                        </div>
                                        <span class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                    </div>
                                </div>
                                <!-- End Single-Product -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SMALL-PRODUCT-AREA -->
            </div>
            <div class="col-md-9">
                <!-- START PRODUCT-BANNER -->
                <div class="product-banner">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="banner">
                                <a href="#"><img src="img/banner/12.jpg" alt="Product Banner"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-BANNER -->
                <!-- Start Shopping-Cart -->
                <?php if ($model): ?>
                <div class="shopping-cart">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cart-title">
                                <h2 class="entry-title">Shopping Cart</h2>
                            </div>
                            <p>
                                <a href="<?php echo Url::to(['/cart/default/clear']); ?>"  class="btn btn-danger" >Удалить все</a></td>
                            </p>
                            <!-- Start Table -->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td class="text-center">Image</td>
                                            <td class="text-left">Product Name</td>
                                            <td class="text-left">Quantity</td>
                                            <td class="text-right">Unit Price</td>
                                            <td class="text-right">Total</td>
                                        </tr>
                                    </thead>
                                    <tbody>                               
    <?php foreach ($model as $product): ?>
                                                <tr>
                                                    <td class="text-center">
                                                        <a href="<?php echo Url::to(['/product/default/view', 'id' => $product['product_id']]); ?>"><img class="img-thumbnail" src="<?php echo $product['image']; ?>" width="90px" alt="#" /></a>
                                                    </td>
                                                    <td class="text-left">
                                                        <a href="<?php echo Url::to(['/product/default/view', 'id' => $product['product_id']]); ?>"><?php echo $product['title']; ?></a>
                                                    </td>
                                                    <td class="text-left">
                                                        <div class="btn-block cart-put">
                                                            <input class="form-control get-qty" type="number" value="<?php echo $product['qty']; ?>" />
                                                            <div class="input-group-btn cart-buttons">
                                                                <button class="btn btn-primary update-qty" data-id="<?php echo $product['product_id']; ?>" data-toggle="tooltip" title="Update">
                                                                    <i class="fa fa-refresh"></i>
                                                                </button>
                                                                <button class="btn btn-danger remove-product" data-id="<?php echo $product['product_id']; ?>" data-toggle="tooltip" title="Remove">
                                                                    <i class="fa fa-times-circle"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-right">$<?php echo $product['price']; ?></td>
                                                    <td class="text-right">$<?php echo $product['price'] * $product['qty']; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- End Table -->
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>Total:</strong>
                                                </td>
                                                <td class="text-right">$<?php echo Cartdb::getTotalSum(); ?></td>
                                            </tr>                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="shopping-checkout">
                                <a href="<?php echo Url::to(['/']); ?>" class="btn btn-default pull-left">Continue Shopping</a>
                                <a href="<?php echo Url::to(['/cart/default/checkout']); ?>"" class="btn btn-primary pull-right">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php else: ?>
                                    <p>Ваша корзина пуста!</p>
                                    <div class="shopping-checkout">
                                <a href="<?php echo Url::to(['/']); ?>" class="btn btn-default pull-left">Continue Shopping</a>    
                            </div>
                                        <?php endif; ?>
                <!-- End Shopping-Cart -->
            </div>
        </div>
    </div>