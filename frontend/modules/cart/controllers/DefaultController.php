<?php

namespace frontend\modules\cart\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Cartdb;
use frontend\modules\cart\models\CartdbFindModel;
use frontend\modules\cart\models\CartSession;
use frontend\modules\cart\models\forms\OrderForm;
use frontend\modules\product\models\ProductFindModel;

/**
 * Default controller for the `cart` module
 */
class DefaultController extends Controller
{
    
//    public function actionCheck()
//    {
//        $evt = new ProductBoughtEvent($_SESSION['cart']);
//        $product = new Product();
//        $product->updateProductBestsellerList($evt);
//        debug($evt->getProductList());
//    }
    
    public function actionCheckout()
    {
        if(Yii::$app->user->isGuest) {
            return $this->checkoutSession();            
        }
        return $this->checkoutDb();
    }
    
    public function checkoutSession()
    {
        $session = Yii::$app->session;
        $session->open();
        $model = new OrderForm();
        if($model->load(Yii::$app->request->post())) {
            $userNotRegistered = 0;
            $model->user_id = $userNotRegistered;
            $model->qty = $_SESSION['qty'];
            $model->total = $_SESSION['sum'];
            if($model->save($_SESSION['cart'])) {
                Yii::$app->session->setFlash('success', 'Ваш заказ принят!');
                $this->actionClear();
                return $this->redirect('/');
            }
            Yii::$app->session->setFlash('error', 'Ошибка оформления заказа');
            return $this->redirect('/');
        }
        return $this->render('checkout-session', [
            'session' => $session,
            'model' => $model
        ]);
    }
    
    public function checkoutDb()
    {
        
        $model = new OrderForm();
        $products = CartdbFindModel::findCart(Yii::$app->user->identity);
        if($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->identity->id;
            $model->qty = Cartdb::getTotalQty();
            $model->total = Cartdb::getTotalSum();
            if($model->save($products)) {
                Yii::$app->session->setFlash('success', 'Ваш заказ принят!');
                $this->actionClear();
                return $this->redirect('/');
            }
            Yii::$app->session->setFlash('error', 'Ошибка оформления заказа');
            return $this->redirect('/');
        }
        return $this->render('checkout-db', [
            'products' => $products,
            'model' => $model,
        ]);
    }
    
    public function actionAdd($id)
    {
        $product = ProductFindModel::findProduct($id);
        if(Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            $session->open();
            $model = new CartSession();
            $model->addToCart($product);
            if(Yii::$app->request->isAjax) {
        
                return $this->renderPartial('cart-session-modal', compact('session'));
            }
        } else {
            $model = new Cartdb();
            $model->addToCart($product);
            if(Yii::$app->request->isAjax) {
        
                return $this->renderPartial('cart-db-modal', [
                    'model' => CartdbFindModel::findCart(Yii::$app->user->identity)
                ]);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(Yii::$app->user->isGuest) {
            $model = new CartSession();
            $model->delete($id);
        } else {
            $model = CartdbFindModel::findProductInCartById($id);
            $model->delete();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionUpdate()
    {
        $id = Yii::$app->request->get('id');
        $qty = Yii::$app->request->get('qty');
        
        if(Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            $session->open();
            $model = new CartSession();
            $model->update($id, $qty);
        } else {        
            $model = CartdbFindModel::findProductInCartById($id);
            $model->updateQty($qty);
        }
        return $this->redirect(Yii::$app->request->referrer); 
    }
    
    public function actionView()
    {
        if(Yii::$app->user->isGuest) {
            return $this->render('session-view');
        }
        return $this->render('db-view', [
            'model' => CartdbFindModel::findCart(Yii::$app->user->identity)
        ]);
    }
    
    public function actionShow()
    {
        if(Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            $session->open();
            return $this->renderPartial('cart-session-modal', compact('session'));
        }
        
        return $this->renderPartial('cart-db-modal', [
            'model' => CartdbFindModel::findCart(Yii::$app->user->identity)
        ]);
    }
    
    public function actionClear()
    {
        if(Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            $session->open();
            $model = new CartSession();
            $model->clear($session);
        } else {
            Cartdb::clear(Yii::$app->user->identity->id);
        }
        return $this->redirect('/');
    }
    
}
