<?php

namespace frontend\modules\comment\models\forms;

use yii\base\Model;
use frontend\models\User;
use yii\behaviors\TimestampBehavior;
use frontend\models\Comment;
use frontend\modules\product\models\Product;

/**
 * Description of CommentForm
 *
 * @author grigo
 */
class CommentForm extends Model
{
    
    private $user;
    private $product;
    public $text;
    
    public function __construct(User $user, Product $product)
    {
        $this->user = $user;
        $this->product = $product;
    }
    
    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string', 'min' => 2, 'max' => '1000'],
        ];
    }
    
    public function save()
    {
        if($this->validate()) {
            $comment = new Comment();
            $comment->user_id = $this->user->id;
            $comment->product_id = $this->product->id;
            $comment->author_name = $this->user->username;
            $comment->text = $this->text;
            if($comment->save(false)) {
    //            $this->product->commentIncrement();
    //            $this->user->commentIncrement();
                return true;
            }            
        }
    }
}
