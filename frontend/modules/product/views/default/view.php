<?php

use yii\web\JqueryAsset;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
?>
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
<!--        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="<?php // echo Url::to(['/']); ?>">Home</a></li>
                    <li><a href="#">cameras & photography</a></li>
                    <li class="active"><a href="#">Toch Prond</a></li>
                </ul>
            </div>
        </div>-->
        <?php echo \frontend\widgets\breadcrumbs\BreadcrumbsWidget::widget(['category_id' => $product->category_id, 'title' => $product->title]); ?>
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
                <?php echo $this->render('//layouts/inc/sidebar.php'); ?>	
                <!-- END CATEGORY-MENU-LIST -->
                <!-- START SMALL-PRODUCT-AREA -->
                <?php echo \frontend\widgets\bestseller\BestsellerWidget::widget(); ?>
                <!-- END SMALL-PRODUCT-AREA -->
                <!-- START SIDEBAR-BANNER -->
                <div class="sidebar-banner hidden-sm hidden-xs">
                    <div class="active-sidebar-banner">
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="img/banner/1.jpg" alt="Product Banner"></a>
                        </div>
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="img/banner/2.jpg" alt="Product Banner"></a>
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR-BANNER -->
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <!-- Start Toch-Prond-Area -->
                <div class="toch-prond-area">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="clear"></div>
                            <div class="tab-content">
                                <!-- Product = display-1-1 -->
                                <?php if ($product->getSlider()): ?>
                                    <?php $i = 1;
                                    foreach ($product->getSlider() as $slider): ?>
                                        <div role="tabpanel" class="tab-pane fade in <?php if ($i == 1) : echo 'active';
                                endif; ?>" id="display-<?php echo $i; ?>">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="toch-photo">
                                                        <a href="#"><img src="<?php echo $slider; ?>" data-imagezoom="true" alt="#" /></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php $i++;
                                    endforeach; ?>
<?php else: ?>
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-1">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="toch-photo">
                                                    <a href="#"><img src="<?php echo $product->getImage(); ?>" data-imagezoom="true" alt="#" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
<?php endif; ?>
                                <!-- End Product = display-1-1 -->
                            </div>
                            <!-- Start Toch-prond-Menu -->
                            <div class="toch-prond-menu">
                                <ul role="tablist">
<?php if ($product->getSlider()): ?>
    <?php $i = 1;
    foreach ($product->getSlider() as $slider): ?>
                                            <li role="presentation" class=" <?php if ($i == 1) : echo 'active';
        endif; ?>">
                                                <a href="#display-<?php echo $i; ?>" role="tab" data-toggle="tab">
                                                    <img src="<?php echo $slider; ?>" alt="#" />
                                                </a>
                                            </li>
        <?php $i++;
    endforeach; ?>
                                    <?php else: ?>
                                        <li role="presentation" class=" active">
                                            <a href="#display-1" role="tab" data-toggle="tab">
                                                <img src="<?php echo $product->getImage(); ?>" alt="#" />
                                            </a>
                                        </li>     
<?php endif; ?>
                                </ul>
                            </div>
                            <!-- End Toch-prond-Menu -->
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <h2 class="title-product"> <?php echo $product->title; ?></h2>
                            <div class="about-toch-prond">
                                <p>
                                    <span class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="#">1 reviews</a>
                                    /
                                    <a href="#">Write a review</a>
                                </p>
                                <hr />
                                <p class="short-description"><?php echo Yii::$app->stringHelper->getShort($product->description); ?> </p>
                                <hr />
                                <span class="current-price">$<?php echo $product->price; ?></span>
                                <span class="item-stock">Availability: <span class="text-stock">In Stock</span></span>
                            </div>
                            <!-- <div class="about-product">
                                    <div class="product-select product-color">
                                            <label><sup>*</sup>Color</label>
                                            <select class="form-control">
                                                    <option> --- Please Select --- </option>
                                                    <option>Black (+$10.00) </option>
                                                    <option>White (+$8.00)</option>
                                                    <option>Pink (+$30.00)</option>
                                                    <option>Green (+$30.00)</option>
                                            </select>
                                    </div>
                                    <div class="product-select product-Size">
                                            <label><sup>*</sup>Size</label>
                                            <select class="form-control">
                                                    <option> --- Please Select --- </option>
                                                    <option>Small (+$10.00)</option>
                                                    <option>Medium (+$20.00)</option>
                                                    <option>Large (+$30.00)</option>
                                                    <option>Extra Large (+$35.00)</option>
                                            </select>
                                    </div>
                                    <div class="product-select product-type">
                                            <label><sup>*</sup>Text</label>
                                            <input type="text"  class="form-control" placeholder="Text"/>
                                    </div>
                                    <div class="product-select product-date">
                                            <label><sup>*</sup>Date</label>
                                            <input type="text"  class="form-control" placeholder="2016/02/15"/>
                                    </div>
                                    <div class="product-select product-checkbox">
                                            <label><sup>*</sup>Checkbox</label>
                                            <label><input type="checkbox" /> Checkbox 1  (+$5.00)</label>
                                    </div>
                                    <div class="product-select product-button">
                                            <button type="submit" >
                                                    <i class="fa fa-calendar"></i>
                                            </button>
                                    </div>
                            </div> -->
                            <div class="product-quantity">
                                <span>Qty</span>
                                <input type="number" placeholder="1" />
                                <a href="<?php echo Url::to(['/cart/default/add', 'id' => $product->id]); ?>" type="submit" class="toch-button toch-add-cart add-to-cart" data-id="<?php echo $product->id; ?>">Add to Cart</a>
                                <button type="submit" class="toch-button toch-wishlist">wishlist</button>
                                <button type="submit" class="toch-button toch-compare">Compare</button>
                                <hr />
                                <a href="#"><img src="img/icon/social.png" alt="#" /></a>
                            </div>
                        </div>
                    </div>
                    <!-- Start Toch-Box -->
                    <div class="toch-box">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- Start Toch-Menu -->
                                <div class="toch-menu">
                                    <ul role="tablist">
                                        <li role="presentation" class=" active"><a href="#description" role="tab" data-toggle="tab">Description</a></li>
                                        <li role="presentation"><a href="#reviews" role="tab" data-toggle="tab">Reviews (<?php // echo $product->getCommentCounts(); ?>)</a></li>
                                    </ul>
                                </div>
                                <!-- End Toch-Menu -->
                                <div class="tab-content toch-description-review">
                                    <!-- Start display-description -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="description">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="toch-description">
                                                    <p>
<?php echo $product->description; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End display-description -->
                                    <!-- Start display-reviews -->
                                    <div role="tabpanel" class="tab-pane fade" id="reviews">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="toch-reviews">
                                                    <?php Pjax::begin(); ?>
<?php
//                                                                                                echo CommentWidget::widget([
//                                                                                                    'product' => $product,
//                                                                                                ]); 
?>                                                  <?php if($product->getComments()): ?>
                                                    <?php foreach($product->getComments() as $comment): ?>
                                                    <div class="toch-table">
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td><strong><?php echo $comment->author_name; ?></strong></td>
                                                                    <td class="text-right"><strong><?php echo Yii::$app->formatter->asDate($comment->created_at); ?></strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <p><?php echo $comment->text; ?></p>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star-o"></i></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    <?php if (Yii::$app->user->identity && Yii::$app->user->identity->id == $comment->user_id): ?>
                                                            <p><a href="<?php echo Url::to(['/comment/default/delete', 'id' => $comment->id]); ?>">Удалить комментарий</a></p>
<?php endif; ?>
                                                    </div>
                                                    <?php endforeach; ?>
                                                    <?php endif; ?>
                                                            <?php if (!Yii::$app->user->isGuest): ?>
                                                        <div class="toch-review-title">
                                                            <h2>Write a review</h2>
                                                        </div>
                                                        <div class="review-message">
                                                            <div class="col-xs-12">
    <?php
    $form = yii\widgets\ActiveForm::begin([
                'method' => 'post',
                'enableAjaxValidation' => false,
                // 'action' => ['/post/default/comment'],
                'options' => [
                    'class' => 'form-horizontal',
                    'data-pjax' => true,
                ],
    ]);
    ?>
    <!--														<p><sup>*</sup>Your Name <br>
                                                                            <textarea class="form-control"></textarea>
                                                                    </p>-->
    <?php echo $form->field($commentForm, 'text')->textarea(['class' => 'form-control'])->label('Your Text'); ?>

                                                            </div>
                                                            <div class="help-block">
                                                                <span class="note">Note:</span>
                                                                HTML is not translated!
                                                            </div>
                                                            <div class="get-rating">
                                                                <span><sup>*</sup>Rating </span>
                                                                Bad
                                                                <input type="radio" value="1" name="rating" />
                                                                <input type="radio" value="2" name="rating" />
                                                                <input type="radio" value="3" name="rating" />
                                                                <input type="radio" value="4" name="rating" />
                                                                <input type="radio" value="5" name="rating" />
                                                                Good
                                                            </div>
                                                            <div class="buttons clearfix">
                                                                <!--<button class="btn btn-primary pull-right">Continue</button>-->
    <?php echo Html::submitButton('Continue', ['class' => 'btn btn-primary pull-right']); ?>
                                                            </div>
                                            <?php yii\widgets\ActiveForm::end(); ?>
                                                        </div>
<?php else: ?>
                                                        <p>Вы не можете оставлять комментарии. <a href="<?php echo Url::to(['/user/default/login']); ?>">Авторизуйтесь</a>.</p>
<?php endif; ?>
<?php Pjax::end(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-reviews -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Toch-Box -->
                    <!-- START PRODUCT-AREA -->
                    <div class="product-area">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <!-- Start Product-Menu -->
                                <div class="product-menu">
                                    <div class="product-title">
                                        <h3 class="title-group-2 gfont-1">Related Products</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Product-Menu -->
                        <div class="clear"></div>
                        <!-- Start Product -->
                        <div class="product carosel-navigation">
                            <div class="row">
                                <div class="active-product-carosel">
                                    <!-- Start Single-Product -->
<?php if ($product->category->getProducts()): ?>
    <?php foreach ($product->category->getProducts(6) as $relate): ?>
                                            <div class="col-xs-12">
                                                <div class="single-product">
                                                    <div class="product-img">
                                                        <a href="<?php echo Url::to(['/product/default/view', 'id' => $relate->id]); ?>">
                                                            <img class="primary-img" src="<?php echo $relate->getImage(); ?>" alt="Product">
                                                        </a>
                                                    </div>
                                                    <div class="product-description">
                                                        <h5><a href="<?php echo Url::to(['/product/default/view', 'id' => $relate->id]); ?>"><?php echo $relate->title; ?></a></h5>
                                                        <div class="price-box">
                                                            <span class="price">$<?php echo $relate->price; ?></span>
                                                        </div>
                                                    </div>											
                                                </div>
                                            </div>
    <?php endforeach; ?>
<?php endif; ?>
                                    <!-- End Single-Product -->

                                </div>
                            </div>

                        </div>
                        <!-- End Product -->
                    </div>
                    <!-- END PRODUCT-AREA -->
                </div>
                <!-- End Toch-Prond-Area -->
            </div>
        </div>
    </div>
<?php
$this->registerJsFile('@web/js/imagezoom.js', [
    'depends' => JqueryAsset::class
]);
?>