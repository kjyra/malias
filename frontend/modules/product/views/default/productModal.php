<?php
use yii\helpers\Url;
?>
<!-- Modal -->

            <div class="modal-product">
                <div class="product-images">
                    <div class="main-image images">
                        <img alt="#" src="<?php echo $product->getImage(); ?>"/>
                    </div>
                </div><!-- .product-images -->

                <div class="product-info">
                    <h1><?php echo $product->title; ?></h1>
                    <div class="price-box-3">
                        <hr />
                        <div class="s-price-box">
                            <span class="new-price"><?php echo $product->price; ?></span>
                            <?php if ($product->old_price > $product->price): ?>
                                <span class="old-price">$<?php echo $product->old_price; ?></span>
                            <?php endif; ?>
                        </div>
                        <hr />
                    </div>
                    <a href="<?php echo Url::to(['/']); ?>" class="see-all">See all features</a>
                    <div class="quick-add-to-cart">
                        <!--<form method="post" class="cart">-->
                            <a href="<?php echo Url::to(['/cart/default/add', 'id' => $product->id]); ?>" class="single_add_to_cart_button add-to-cart" data-id="<?php echo $product->id; ?>" type="submit">Add to cart</a>
                        <!--</form>-->
                    </div>
                    <div class="quick-desc">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero.
                    </div>
                    <div class="social-sharing">
                        <div class="widget widget_socialsharing_widget">
                            <h3 class="widget-title-modal">Share this product</h3>
                            <ul class="social-icons">
                                <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .product-info -->
            </div>

<!-- END Modal -->

