<?php
use yii\helpers\Url;
?>

<br>
<?php if($result): ?>
    <div clas="col col-12">
        <?php foreach($result as $product): ?>
        <p><img src="<?php echo $product->getImage(); ?>" width="100px"></p>
        <p><a href="<?php echo Url::to(['/product/default/view', 'id' => $product['id']]); ?>"><?php echo $product['title']; ?></a></p>
        <p><?php echo Yii::$app->stringHelper->getShort($product['description']); ?></p>
        <hr>
        <br>
        <?php endforeach; ?>
    </div>
<?php else: ?>
<h3>По запросу: <?php echo $model->keyword; ?> ничего не найдено</h3>
<?php endif; ?>