<?php

namespace frontend\modules\product\models\interfaces;

/**
 *
 * @author grigo
 */
interface ProductFindInterface
{
    public static function findProduct(int $id);
    public static function findProducts();
    public static function findProductsByCategoriesIds(array $categories);
    public static function findLatestProducts(int $limit = null);
    public static function findRandomProducts(int $limit = null);
    public static function findSaleProducts(int $limit = null);
}
