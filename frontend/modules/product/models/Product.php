<?php

namespace frontend\modules\product\models;

use Yii;
use frontend\modules\category\models\Category;
use frontend\models\Comment;
use frontend\models\interfaces\ProductListInterface;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string $title
 * @property string|null $description
 * @property string|null $image
 * @property float|null $price
 * @property float|null $old_price
 * @property int|null $is_new
 * @property int|null $is_hot
 * @property int|null $status
 */
class Product extends \yii\db\ActiveRecord
{
    
    const DEFAULT_IMAGE = '/img/product/imgonline-com-ua-Resize-PzB9NjQZsU.jpg';
    // const DEFAULT_IMAGE1 = '/img/product/no-product.png';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'is_new', 'is_hot', 'status'], 'integer'],
            [['title'], 'required'],
            [['description'], 'string'],
            [['price', 'old_price'], 'number'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'is_new' => 'Is New',
            'is_hot' => 'Is Hot',
            'status' => 'Status',
        ];
    }
    
    public static function updateProductBestsellerList(ProductListInterface $event)
    {
        foreach($event->getProductList() as $product) {
            $product['product']->bestseller += $product['qty'];
            if(!$product['product']->save(false, ['bestseller'])) {
                return false;
            }                
        }
        return true;
    }
    
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id'])->one();
    }
    
    public function getComments()
    {
        return $this->hasMany(Comment::class, ['product_id' => 'id'])->all();
    }
    
    
    public function getDiscount()
    {
        return 3;
    }
    
    public function getImage()
    {
        if($this->image) {
            return $this->image;
        }
        return self::DEFAULT_IMAGE;
    }
    
    public function getSlider()
    {
        if($this->slider) {
            return json_decode($this->slider);
        }
        return false;
    }
    
    public function commentIncrement()
    {
        $redis = Yii::$app->redis;
        $redis->incr("product:{$this->id}:comments");
    }
    
    public function commentDecrement()
    {
        $redis = Yii::$app->redis;
        $redis->decr("product:{$this->id}:comments");
    }
    
    public function getCommentCounts()
    {
        $redis = Yii::$app->redis;
        return $redis->get("product:{$this->id}:comments");
    }
}
