<?php

namespace frontend\modules\product\models;

use yii\web\NotFoundHttpException;
use frontend\modules\product\models\Product;
use frontend\modules\product\models\interfaces\ProductFindInterface;
use frontend\modules\category\models\Category;
/**
 * Description of ProductModel
 *
 * @author grigo
 */
class ProductFindModel implements ProductFindInterface
{
    /**
     * 
     * @param int $id
     * @return frontend\modules\product\models\Product
     * @throws NotFoundHttpException
     */
    public static function findProduct(int $id)
    {
        if($product = Product::findOne($id)) {
            return $product;
        }
        throw new NotFoundHttpException();
    }
    
//    public static function findProductWithCategory(int $id)
//    {
//        if($product = Product::find()->where(['id' => $id])->with(Category::class)->one()) {
//            return $product;
//        }
//        throw new NotFoundHttpException();
//    }
    
    /**
     * 
     * @return array frontend\modules\product\models\Product
     * @throws NotFoundHttpException
     */
    public static function findProducts()
    {
        if($products = Product::find()->where(['status' => 1])->all()) {
            return $products;
        }
        throw new NotFoundHttpException();
    }
    
    public static function findProductsByCategoriesIds(array $categories)
    {
        if($categories) {
            $ids = [];
            foreach($categories as $category) {
                $ids[] = $category->id;
            }
        }
        if($products = Product::find()->where(['in', 'category_id', $ids])->all()) {
            return $products;            
        }
        throw new NotFoundHttpException();
    }
    
    /**
     * 
     * @param int $limit
     * @return type
     * @throws NotFoundHttpException
     */
    public static function findLatestProducts(int $limit = null)
    {
        $lim = 15;
        if($limit) {
            $lim = $limit;
        }
        
        if($products = Product::find()->where('status = 1')->orderBy(['created_at' => SORT_DESC])->limit($lim)->all()) {
            return $products;
        }
        throw new NotFoundHttpException();
    }

    public static function findRandomProducts(int $limit = null)
    {
        $lim = 15;
        if($limit) {
            $lim = $limit;
        }
        if($products = Product::find()->where('status = 1')->limit($lim)->all()) {
            return $products;
        }
        throw new NotFoundHttpException();
    }

    public static function findSaleProducts(int $limit = null)
    {
        $lim = 15;
        if($limit) {
            $lim = $limit;
        }
        if($products = Product::find()->where('old_price > price AND status = 1')->limit($lim)->all()) {
            return $products;
        }
        throw new NotFoundHttpException();
    }
    
    public static function findSortedProducts($sort, $limit, $categoryId)
    {
        return Product::find()->where(['category_id' => $categoryId])->orderBy($sort)->limit($limit);
    }
    
    public static function findBestsellerProducts(int $limit)
    {
        if($products = Product::find()->where(['status' => 1])->orderBy(['bestseller' => SORT_DESC])->limit($limit)->all()) {
            return $products;
        }
        throw new NotFoundHttpException();
    }

}
