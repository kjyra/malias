<?php

namespace frontend\modules\product\models\filter;

use Yii;

/**
 * This is the model class for table "attribute_value".
 *
 * @property int $id
 * @property int|null $attr_group_id
 * @property string|null $value
 */
class AttributeValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attr_group_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attr_group_id' => 'Attr Group ID',
            'value' => 'Value',
        ];
    }
}
