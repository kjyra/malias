<?php

namespace frontend\modules\product\models\filter;

use Yii;

/**
 * This is the model class for table "attribute_product".
 *
 * @property int|null $attr_value_id
 * @property int|null $product_id
 */
class AttributeProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attr_value_id', 'product_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'attr_value_id' => 'Attr Value ID',
            'product_id' => 'Product ID',
        ];
    }
}
