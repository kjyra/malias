<?php

namespace frontend\modules\product\models\filter;

use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $id
 * @property string|null $title
 */
class AttributeGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }
    
    public function getAttrs()
    {
        return $this->hasMany(AttributeValue::class, ['attr_group_id' => 'id'])->all();
    }
    
}
