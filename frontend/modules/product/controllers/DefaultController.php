<?php

namespace frontend\modules\product\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\product\models\ProductFindModel;
use frontend\modules\comment\models\forms\CommentForm;

/**
 * Default controller for the `product` module
 */
class DefaultController extends Controller
{
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action);
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $products = ProductModel::findProducts();
        return $this->render('index');
    }
    
    public function actionSearch()
    {
        $model = new \frontend\widgets\search\forms\SearchForm();

        if($model->load(Yii::$app->request->post())) {
            $result = $model->search();
        }
        return $this->render('search', [
            'model' => $model,
            'result' => $result
        ]);
    }
    
    public function actionView(int $id)
    {
        $product = ProductFindModel::findProduct($id);
        if(!Yii::$app->user->isGuest) {
            $commentForm = new CommentForm(Yii::$app->user->identity, $product);
            if(Yii::$app->request->isPjax) {
                if($commentForm->load(Yii::$app->request->post())) {
                    if($commentForm->save()) {
                        Yii::$app->session->setFlash('success', 'Commented!');
                        $commentForm = new CommentForm(Yii::$app->user->identity, $product);
                    }
                }
            }
        } else {
            $commentForm = null;
        }
        return $this->render('view', compact('product', 'commentForm'));
    }
    
    public function actionProductModal()
    {
        $productId = Yii::$app->request->get('id');
        // debug($productId);
        $product = ProductFindModel::findProduct($productId);
        return $this->renderPartial('productModal', [
            'product' => $product
        ]);
    }
    
//    public function actionGenerate()
//    {
//        $faker = \Faker\Factory::create();
//        for($i = 0; $i < 8; $i++) {
//            $product = new \frontend\modules\product\models\Product([
//                'category_id' => 11,
//                'title' => $faker->name,
//                'description' => $faker->text,
//                'price' => mt_rand(4, 60),
//                'old_price' => mt_rand(6, 80),
//                'is_new' => mt_rand(0, 1),
//                'is_hot' => mt_rand(0, 1)
//            ]);
//            $product->save(false);
//        }        
//    }
}
