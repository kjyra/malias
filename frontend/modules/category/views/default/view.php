<?php

use yii\web\JqueryAsset;
use yii\helpers\Url;
use yii\widgets\LinkPager;
//echo $category->id;
?>
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
<!--        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="<?php echo Url::to(['/']); ?>">Home</a></li>
                    <li class="active"><a href="/">Bestseller Product</a></li>
                </ul>
            </div>
        </div>-->

        <?php echo \frontend\widgets\breadcrumbs\BreadcrumbsWidget::widget(['category_id' => $category->id]); ?>

        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->               
<?php echo $this->render('//layouts/inc/sidebar.php'); ?>
                <!-- END CATEGORY-MENU-LIST -->
                <?php // echo frontend\widgets\bestseller\BestsellerWidget::widget(); ?>
                <?php echo \frontend\widgets\filter\FilterWidget::widget(); ?>
            </div>
            <div class="col-md-9 col-xs-12">
                <!-- START PRODUCT-BANNER -->
                <div class="product-banner">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="banner">
                                <a href="#"><img src="img/banner/6.jpg" alt="Product Banner"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-BANNER -->
                <!-- START PRODUCT-AREA -->
                <div class="product-area">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Start Product-Menu -->
                            <div class="product-menu">
                                <div class="product-title">
                                    <h3 class="title-group-3 gfont-1"><?php echo $category->title; ?></h3>
                                </div>
                            </div>
                            <div class="product-filter">
                                <ul role="tablist">
                                    <li role="presentation" class="list">
                                        <a href="#display-1-1" role="tab" data-toggle="tab"></a>
                                    </li>
                                </ul>
                                <?php echo frontend\widgets\sort\SortWidget::widget(); ?>
                            </div>
                            <!-- End Product-Menu -->
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">		
                            <!-- Start Product -->
                            <div class="product">
                                <div class="tab-content">
                                    <!-- Product -->

                                    <!-- End Product -->
                                    <!-- Start Product-->
                                    <div role="tabpanel" class="tab-pane fade in  active" id="display-1-2">
                                        <div class="row">
                                            <?php if($products): ?>
<?php $i = 0; foreach ($products as $product): ?>
                                                <!-- Start Single-Product -->
                                                <div class="col-md-3 col-sm-4 col-xs-12">
                                                    <div class="single-product">
                                                        <div class="label_new">
                                                            <span class="new">new</span>
                                                        </div>
    <?php if ($product->old_price > $product->price): ?>
                                                            <div class="sale-off">
                                                                <span class="sale-percent"><?php echo $product->getDiscount(); ?></span>
                                                            </div>
    <?php endif; ?>
                                                        <div class="product-img">
                                                            <a href="<?php echo Url::to(['/product/default/view', 'id' => $product->id]); ?>">
                                                                <img class="primary-img" src="<?php echo $product->getImage(); ?>" alt="Product">
                                                                <?php if ($product->secondary_img): ?>
                                                                    <img class="secondary-img" src="<?php echo $product->secondary_img; ?>" alt="Product">
    <?php endif; ?>
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="#"><?php echo $product->title; ?></a></h5>
                                                            <div class="price-box">
                                                                <span class="price"><?php echo $product->price; ?></span>
                                                                <?php if ($product->old_price > $product->price): ?>
                                                                    <span class="old-price"><?php echo $product->old_price; ?></span>
    <?php endif; ?>
                                                            </div>
                                                            <span class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>
                                                        </div>
                                                        <div class="product-action">
                                                            <div class="button-group">
                                                                <div class="product-button">
                                                                    <a class="add-to-cart" href="<?php echo Url::to(['/cart/default/add', 'id' => $product->id]); ?>" 
                                                                       data-id="<?php echo $product->id; ?>">
                                                                        <i class="fa fa-shopping-cart add-to-cart">
                                                                            
                                                                        </i> 
                                                                        Add to Cart
                                                                    </a>
                                                                </div>
                                                                <div class="product-button-2">
                                                                    <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                    <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                    <a href="#" class="modal-view" data-id="<?php echo $product->id; ?>" data-toggle="modal" data-target="#productModal"><i class="fa fa-search-plus"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>												
                                                    </div>
                                                </div>
                                                <!-- End Single-Product -->
<?php $i++; endforeach; ?>
                                                <?php else: ?>
                                                <br>
                                                <div class="col-md-12">
                                                    <h4>У этой категории нет продуктов</h4>
                                                </div>
                                                <?php endif; ?>
                                        </div>
                                        <!-- Start Pagination Area -->
                                        <div class="pagination-area">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <div class="pagination">
                                                        <ul>
                                                            <?php 
                                                            echo LinkPager::widget([
                                                                'pagination' => $pages,
//                                                                'params' => [],
                                                            ]);
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="product-result">
                                                        <?php if ($i + $pages->offset >= $pages->offset + 1): ?>
                                                            <span>Showing <?php echo $pages->offset + 1; ?> to <?php echo $i + $pages->offset; ?> of <?php echo $countQuery->count(); ?> (<?php echo $pages->pageCount; ?> Pages)</span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Pagination Area -->
                                    </div>
                                    <!-- End Product = TV -->
                                </div>
                            </div>
                            <!-- End Product -->
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-AREA -->
            </div>
        </div>
    </div>
    <?php
    $this->registerJsFile('@web/js/product-modal.js', [
        'depends' => JqueryAsset::class
    ]);
    ?>
    <?php 
    $this->registerJsFile('@web/js/filter.js',[ 
        'depends' => \yii\web\JqueryAsset::class
    ]);
    ?>
    <?php 
    $this->registerJsFile('@web/js/sort.js',[ 
        'depends' => \yii\web\JqueryAsset::class
    ]);
    ?>