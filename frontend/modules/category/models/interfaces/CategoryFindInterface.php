<?php

namespace frontend\modules\category\models\interfaces;

/**
 *
 * @author grigo
 */
interface CategoryFindInterface
{
    public static function findCategory($id);
    public static function findCategories();
    public static function findCategoriesByTitle(array $array);
}
