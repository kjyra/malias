<?php

namespace frontend\modules\category\models\sort;

use Yii;

/**
 * This is the model class for table "sort".
 *
 * @property int $id
 * @property int|null $number
 * @property string|null $title
 */
class Sort extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sort';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'title' => 'Title',
        ];
    }
}
