<?php

namespace frontend\modules\category\models;

use Yii;
use frontend\modules\product\models\Product;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $title
 * @property string|null $description
 * @property int $status
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'status'], 'integer'],
            [['title'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
    
    public function getParent()
    {
        return self::find()->where(['id' => $this->parent_id])->one();
    }
    
    public function hasChildren()
    {
        if($this->children == null) {
            return false;
        }
        return true;
    }
    
    public function getChildren(int $limit = null)
    {
        if($limit) {
            return self::find()->where(['parent_id' => $this->id])->limit($limit)->all();
        }
        return self::find()->where(['parent_id' => $this->id])->all();
    }
    
    public function getChildrenById($id)
    {
        return self::findOne($id);
    }
    
    public function getParents()
    {
        if($this->parent_id > 0) {
            $parents = [$parent = $this->getParent()];
            while($parent->parent_id > 0) {
                $parents[] = $parent->getParent();
                $parent = $parent->getParent();
            }
            return $parents;
        }
        return false;
    }
    
    public function getDescendants()
    {
        if($this->getChildren()) {
            $descendants = [$children = $this->getChildren()];
            foreach($children as $child) {
                while($child->getChildren()) {
                    $descendants[] = $child->getChildren();
                    $child = $child->getChildren();
                }
            }
            return $descendants;
        }
        return false;
    }
    
    public function getProducts(int $limit = null)
    {
        if($limit) {
            return $this->hasMany(Product::class, ['category_id' => 'id'])->limit($limit)->all();            
        }
         return $this->hasMany(Product::class, ['category_id' => 'id'])->all();
    }

}
