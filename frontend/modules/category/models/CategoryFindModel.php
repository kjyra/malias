<?php

namespace frontend\modules\category\models;

use yii\web\NotFoundHttpException;
use frontend\modules\category\models\Category;
use frontend\modules\category\models\interfaces\CategoryFindInterface;
/**
 * Description of CategoryModel
 *
 * @author grigo
 */
class CategoryFindModel implements CategoryFindInterface
{
    
    public static function findCategory($id)
    {
        if($category = Category::findOne($id)) {
            return $category;
        }
        throw new NotFoundHttpException();
    }

    public static function findCategories()
    {
        if($categories = Category::find()->where('status = 1')->all()) {
            return $categories;
        }
        throw new NotFoundHttpException();
    }
    
    public static function findRootCategories(int $limit)
    {
        if($categories = Category::find()->where('status = 1')->andWhere('parent_id = 0')->limit($limit)->all()) {
            return $categories;
        }
        throw new NotFoundHttpException();
    }
    
    public static function findCategoriesByTitle(array $categoriesTitle)
    {
        if($categories = Category::find()->where(['in', 'title', $categoriesTitle])->all()) {
            return $categories;
        }
        throw new NotFoundHttpException();
    }

}
