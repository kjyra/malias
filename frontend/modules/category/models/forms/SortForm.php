<?php

namespace frontend\modules\category\models\forms;

use yii\base\Model;

/**
 * Description of SortForm
 *
 * @author grigo
 */
class SortForm extends Model
{
    
    public $show;
    public $sortBy;
    
    public function rules()
    {
        return [
            
            ['show', 'integer'],
            ['sortBy', 'string'],
        ];
    }
}
