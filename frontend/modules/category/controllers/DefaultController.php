<?php

namespace frontend\modules\category\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\category\models\CategoryFindModel;
use frontend\modules\product\models\Product;
use yii\data\Pagination;

/**
 * Description of DefaultController
 *
 * @author grigo
 */
class DefaultController extends Controller
{   
    
    public function actionView(int $id)
    {
        $sql_part = '';
        $sort = '';
        if(!empty(Yii::$app->request->get())) {
            if(!empty(Yii::$app->request->get('filter'))) {
                $filter = \frontend\widgets\filter\FilterWidget::getFilter();
                if($filter) {
                    $cnt = \frontend\widgets\filter\FilterWidget::getCountGroups($filter);
                    $sql_part = "id IN (SELECT product_id FROM attribute_product WHERE attr_value_id IN ($filter) "
                            . "GROUP BY product_id HAVING COUNT(product_id) = $cnt)";             
                }
            
            // SELECT product_id FROM attribute_product WHERE attr_value_id IN (5)
            }
            
            if(!empty(Yii::$app->request->get('sort'))) {
                $sort = "" . $this->switchSort(Yii::$app->request->get('sort')) . "";
            }
        }

        $category = CategoryFindModel::findCategory($id);
        $query = Product::find()->where(['category_id' => $id])->andWhere($sql_part)->orderBy($sort);
        /* SELECT * FROM product WHERE category_id = 1 
         * AND id IN(SELECT product_id FROM attribute_product WHERE attr_value_id IN (3));
        */

//        debug($query, 1);
        
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 
            'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        
        if(Yii::$app->request->isAjax) {
            return $this->renderPartial('filter', [
            'products' => $products,
            'pages' => $pages,
            'countQuery' => $countQuery
        ]);
//            exit;
        }
        
        return $this->render('view', [
            'category' => $category,
            'products' => $products,
            'pages' => $pages,
            'countQuery' => $countQuery
        ]);
    }
    
    private function switchSort($sortNumber)
    {
        $sortModel = \frontend\modules\category\models\sort\Sort::find()->asArray()->all();
        foreach($sortModel as $sort) {
            if($sortNumber && in_array($sortNumber, $sort)) {
                return $sort['value'];
            }
        }
        return false;
    }    
    
}
