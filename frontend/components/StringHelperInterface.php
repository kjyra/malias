<?php

namespace frontend\components;

/**
 *
 * @author grigo
 */
interface StringHelperInterface
{
    public function getShort($text, $limit = null);
}
