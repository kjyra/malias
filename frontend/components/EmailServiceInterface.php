<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\components;

use frontend\models\interfaces\NotifyUserInterface;

/**
 *
 * @author grigo
 */
interface EmailServiceInterface
{
    public function notifyUser(NotifyUserInterface $event);
    public function notifyAdmin(NotifyUserInterface $event);
}
