<?php

namespace frontend\components;

use yii\base\BootstrapInterface;

/**
 * Description of LanguageSelector
 *
 * @author grigo
 */
class LanguageSelector implements BootstrapInterface
{
    public $supportedLanguages = ['ru-RU', 'en-US'];
    
    public function bootstrap($app)
    {
        $cookieLanguage = $app->request->cookies['language'];
        if($cookieLanguage && in_array($cookieLanguage, $this->supportedLanguages)) {
            $app->language = $app->request->cookies['language'];
        }
    }
}
