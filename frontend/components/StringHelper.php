<?php

namespace frontend\components;

use Yii;
use yii\base\Component;

/**
 * Description of StringHelper
 *
 * @author grigo
 */
class StringHelper extends Component implements StringHelperInterface
{
    
    public function getShort($text, $limit = null)
    {
        $count = Yii::$app->params['defaultTextSizeLimit'];
        if($limit) {
            $count = $limit;
        }
        $text = mb_substr($text, 0, $count);
        return $text;
    }
}
