<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\components;

use Yii;
use frontend\models\interfaces\NotifyUserInterface;
/**
 * Description of EmailService
 *
 * @author grigo
 */
class EmailService implements EmailServiceInterface
{

    public function notifyAdmin(NotifyUserInterface $event)
    {
        return Yii::$app->mailer->compose()
            ->setFrom('service.email@malias.com')
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject($event->getSubject())
            ->send();
    }

    public function notifyUser(NotifyUserInterface $event)
    {
        return Yii::$app->mailer->compose()
            ->setFrom('service.email@malias.com')
            ->setTo($event->getEmail())
            ->setSubject($event->getSubject())
            ->send();
    }

}
