<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models\events;

use yii\base\Event;
use frontend\models\interfaces\ProductListInterface;
use frontend\modules\product\models\ProductFindModel;

/**
 * Description of ProductBoughtEvent
 *
 * @author grigo
 */
class ProductBoughtEvent extends Event implements ProductListInterface
{
    
    public $products;
    
    public function __construct($products)
    {
        $this->products = $products;
    }
    
    public function getProductList()
    {
        $products = [];
        $i = 1;
        foreach($this->products as $product) {
            $products[$i]['qty'] = $product['qty'];
            $products[$i]['product'] = ProductFindModel::findProduct($product['product_id']);
            $i++;
        }
        return $products;
    }
}
