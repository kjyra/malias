<?php

namespace frontend\models\events;

use yii\base\Event;

use frontend\models\interfaces\NotifyUserInterface;

/**
 * Description of OrderCreatedEvent
 *
 * @author grigo
 */
class OrderCreatedEvent extends Event implements NotifyUserInterface
{
    public $email;
    public $subject;
    
    public function __construct($email, $subject)
    {
        $this->email = $email;
        $this->subject = $subject;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function getSubject()
    {
        return $this->subject;
    }
}
