<?php

namespace frontend\models;

use Yii;
use frontend\modules\product\models\Product;

/**
 * This is the model class for table "cartdb".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $product_id
 * @property string|null $image
 * @property float|null $price
 * @property int|null $qty
 */
class Cartdb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cartdb';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id', 'qty'], 'integer'],
            [['price'], 'number'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'image' => 'Image',
            'price' => 'Price',
            'qty' => 'Qty',
        ];
    }
    
    public function addToCart(Product $product, $qty = 1)
    {
        if($cartProduct = self::find()->where(['product_id' => $product->id])->one()) {
            $cartProduct->qty++;
            return $cartProduct->save(false, ['qty']);
        } else {
            $this->user_id = Yii::$app->user->identity->id;
            $this->product_id = $product->id;
            $this->title = $product->title;
            $this->image = $product->getImage();
            $this->price = $product->price;
            $this->qty = $qty;      
            return $this->save();
        }
        
    }
    
    public function updateQty($qty)
    {
        if($qty <= 0) {
            return $this->delete();
        }
        $this->qty = $qty;
        return $this->save(false, ['qty']);
    }
    
    public static function clear($id)
    {
        self::deleteAll(['user_id' => $id]);
    }
    
    public static function getTotalSum()
    {
        $sum = 0;
        $userCart = self::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        foreach($userCart as $product) {
            $sum += $product->price * $product->qty;
        }
        return $sum;
    }
    
    public static function getTotalQty()
    {
        $qty = 0;
        $userCart = self::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        foreach($userCart as $product) {
            $qty += $product->qty;
        }
        return $qty;
    }
}
