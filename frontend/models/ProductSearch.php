<?php

namespace frontend\models;

use Yii;
use frontend\modules\product\models\Product;

/**
 * Description of ProductSearch
 *
 * @author grigo
 */
class ProductSearch
{
    
    public function simpleSearch($keyword)
    {
        $sql = "SELECT * FROM product WHERE status = 1 AND title LIKE '%$keyword%' LIMIT 10";
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }
    
    public function fulltextSearch($keyword, $cat_id = 0)
    {
        $sql = "SELECT * FROM product WHERE status = 1 AND category_id = $cat_id AND MATCH (title) AGAINST ('$keyword') LIMIT 10";
        if($cat_id == 0) {
            $sql = "SELECT * FROM product WHERE status = 1 AND MATCH (title) AGAINST ('$keyword') LIMIT 10";
        }
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }
    
    public function fulltextSearchActiveRecord($keyword, $cat_id = 0)
    {
        if($cat_id == 0) {
            $result = Product::find()->where('status = 1')->andWhere(['like', 'title', $keyword . '%', false])->all();
        } else {            
            $result = Product::find()->where('status = 1')->andWhere(['like', 'title', $keyword . '%', false])->andWhere(['category_id' => $cat_id])->all();
        }
        return $result;
    }
}
