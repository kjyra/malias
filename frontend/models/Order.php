<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $qty
 * @property float|null $total
 * @property int|null $status
 * @property string|null $name
 * @property string|null $email
 * @property string|null $number
 * @property string|null $text
 * @property string|null $address
 */
class Order extends ActiveRecord
{
    
    const NEW_ORDER_STATUS = 0;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'qty', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['total'], 'number'],
            [['text'], 'string'],
            [['name', 'email', 'number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'qty' => 'Qty',
            'total' => 'Total',
            'status' => 'Status',
            'name' => 'Name',
            'email' => 'Email',
            'number' => 'Number',
            'text' => 'Text',
        ];
    }
}
