<?php

namespace frontend\models\interfaces;

/**
 *
 * @author grigo
 */
interface NotifyUserInterface
{
    public function getEmail();
    public function getSubject();
}
