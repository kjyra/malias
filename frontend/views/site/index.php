<?php

/* @var $this yii\web\View */
/* @var $categories frontend\modules\category\models\Category */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JqueryAsset;
//debug($_SESSION);
//session_destroy();
$this->title = 'index';
?>

<!-- HEADER AREA END -->
<!-- Category slider area start -->
<div class="category-slider-area">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php echo $this->render('//layouts/inc/sidebar.php'); ?>
            </div>
            <div class="col-md-9">
                <!-- slider -->
                <div class="slider-area">
                    <div class="bend niceties preview-1">
                        <div id="ensign-nivoslider" class="slides">	
                            <img src="img/sliders/slider-1/bg1.jpg" alt="Malias" title="#slider-direction-1"/>
                            <img src="img/sliders/slider-1/bg2.jpg" alt="Malias" title="#slider-direction-2"/>
                            <img src="img/sliders/slider-1/bg3.jpg" alt="Malias" title="#slider-direction-3"/><!-- 
                            <img src="img/sliders/slider-1/bg4.jpg" alt="" title="#slider-direction-4"/>  -->     
                        </div>
                        <!-- direction 1 -->
                        <div id="slider-direction-1" class="t-lfr slider-direction">
                            <div class="slider-progress"></div>
                            <!-- layer 1 -->
                            <div class="layer-1-1 ">
                                <h1 class="title1">LUMIA 888 DESIGN</h1>
                            </div>
                            <!-- layer 2 -->
                            <div class="layer-1-2">
                                <p class="title2">Elegant design for business</p>
                            </div>
                            <!-- layer 3 -->
                            <div class="layer-1-3">
                                <h2 class="title3">$966.82</h2>
                            </div>
                            <!-- layer 4 -->
                            <div class="layer-1-4">
                                <a href="#" class="title4">shopping now</a>
                            </div>
                        </div>
                        <!-- direction 2 -->
                        <div id="slider-direction-2" class="slider-direction">
                            <div class="slider-progress"></div>
                            <!-- layer 1 -->
                            <div class="layer-2-1">
                                <h1 class="title1">WATERPROOF SMARTPHONE</h1>
                            </div>
                            <!-- layer 2 -->
                            <div class="layer-2-2">
                                <p class="title2">RODUCTS ARE EYE-CATCHING DESIGN. YOU CAN TAKE PHOTOS EVEN WHEN Y</p>
                            </div>
                            <!-- layer 3 -->
                            <div class="layer-2-3">
                                <a href="#" class="title3">shopping now</a>
                            </div>
                        </div>
                        <!-- direction 3 -->
                        <div id="slider-direction-3" class="slider-direction">
                            <div class="slider-progress"></div>
                            <!-- layer 1 -->
                            <div class="layer-3-1">
                                <h2 class="title1">BUY AIR LACOTE</h2>
                            </div>
                            <!-- layer 2 -->
                            <div class="layer-3-2">
                                <h1 class="title2">SUPER TABLET, SUPER GIFT</h1>
                            </div>
                            <!-- layer 3 -->
                            <div class="layer-3-3">
                                <p class="title3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                            </div>
                            <!-- layer 4 -->
                            <div class="layer-3-4">
                                <a href="#" class="title4">shopping now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- slider end-->
            </div>
        </div>
    </div>
</div>
<!-- Category slider area End -->		
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <!-- START HOT-DEALS-AREA -->
                <div class="hot-deals-area carosel-circle">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="area-title">
                                <h3 class="title-group border-red gfont-1">Hot Deals</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="active-hot-deals">
                            <!-- Start Single-hot-deals -->
                            <div class="col-xs-12">
                                <div class="single-hot-deals">
                                    <div class="hot-deals-photo">
                                        <a href="#"><img src="img/hot-deals/1.jpg" alt="Product"></a>
                                    </div>
                                    <div class="count-down">
                                        <div class="timer">
                                            <div data-countdown="2017/12/31"></div>
                                        </div> 
                                    </div>
                                    <div class="hot-deals-text">
                                        <h5><a href="#" class="name-group">Various Versions</a></h5>
                                        <span class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>
                                        <div class="price-box">
                                            <span class="price gfont-2">$99.00</span>
                                            <span class="old-price gfont-2">$110.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-hot-deals -->
                            <!-- Start Single-hot-deals -->
                            <div class="col-xs-12">
                                <div class="single-hot-deals">
                                    <div class="hot-deals-photo">
                                        <a href="#"><img src="img/hot-deals/2.jpg" alt="Product"></a>
                                    </div>
                                    <div class="count-down">
                                        <div class="timer">
                                            <div data-countdown="2017/06/30"></div>
                                        </div> 
                                    </div>
                                    <div class="hot-deals-text">
                                        <h5><a href="#" class="name-group">Trid Palm</a></h5>
                                        <span class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>
                                        <div class="price-box">
                                            <span class="price gfont-2">$85.00</span>
                                            <span class="old-price gfont-2">$120.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-hot-deals -->
                            <!-- Start Single-hot-deals -->
                            <div class="col-xs-12">
                                <div class="single-hot-deals">
                                    <div class="hot-deals-photo">
                                        <a href="#"><img src="img/hot-deals/3.jpg" alt="Product"></a>
                                    </div>
                                    <div class="count-down">
                                        <div class="timer">
                                            <div data-countdown="2017/08/30"></div>
                                        </div> 
                                    </div>
                                    <div class="hot-deals-text">
                                        <h5><a href="#" class="name-group">Established Fact</a></h5>
                                        <span class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>
                                        <div class="price-box">
                                            <span class="price gfont-2">$90.00</span>
                                            <span class="old-price gfont-2">$105.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-hot-deals -->
                        </div>
                    </div>
                </div>
                <!-- END HOT-DEALS-AREA -->
                <!-- START SMALL-PRODUCT-AREA -->
                <?php echo frontend\widgets\bestseller\BestsellerWidget::widget(); ?>
                <!-- END SMALL-PRODUCT-AREA -->                    
                <!-- START SIDEBAR-BANNER -->
                <div class="sidebar-banner">
                    <div class="active-sidebar-banner">
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="img/banner/1.jpg" alt="Product Banner"></a>
                        </div>
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="img/banner/2.jpg" alt="Product Banner"></a>
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR-BANNER -->
                <!-- START RECENT-POSTS -->
                <div class="shop-blog-area sidebar">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-group border-red gfont-1">RECENT POSTS </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="active-recent-posts carosel-circle">
                            <!-- Start Single-Recent-Posts -->
                            <div class="col-xs-12">
                                <div class="single-recent-posts">
                                    <div class="recent-posts-photo">
                                        <img src="img/posts/1.jpg" alt="Recent Posts">
                                    </div>
                                    <div class="recent-posts-text">
                                        <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                                        <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                                        <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                        <a href="#" class="posts-read-more">Read more ...</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Recent-Posts -->
                            <!-- Start Single-Recent-Posts -->
                            <div class="col-xs-12">
                                <div class="single-recent-posts">
                                    <div class="recent-posts-photo">
                                        <img src="img/posts/2.jpg" alt="Recent Posts">
                                    </div>
                                    <div class="recent-posts-text">
                                        <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                                        <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                                        <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                        <a href="#" class="posts-read-more">Read more ...</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Recent-Posts -->
                            <!-- Start Single-Recent-Posts -->
                            <div class="col-xs-12">
                                <div class="single-recent-posts">
                                    <div class="recent-posts-photo">
                                        <img src="img/posts/3.jpg" alt="Recent Posts">
                                    </div>
                                    <div class="recent-posts-text">
                                        <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                                        <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                                        <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                        <a href="#" class="posts-read-more">Read more ...</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Recent-Posts -->
                            <!-- Start Single-Recent-Posts -->
                            <div class="col-xs-12">
                                <div class="single-recent-posts">
                                    <div class="recent-posts-photo">
                                        <img src="img/posts/4.jpg" alt="Recent Posts">
                                    </div>
                                    <div class="recent-posts-text">
                                        <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                                        <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                                        <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                        <a href="#" class="posts-read-more">Read more ...</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Recent-Posts -->
                            <!-- Start Single-Recent-Posts -->
                            <div class="col-xs-12">
                                <div class="single-recent-posts">
                                    <div class="recent-posts-photo">
                                        <img src="img/posts/1.jpg" alt="Recent Posts">
                                    </div>
                                    <div class="recent-posts-text">
                                        <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                                        <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                                        <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                                        <a href="#" class="posts-read-more">Read more ...</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Recent-Posts -->
                        </div>
                    </div>
                </div>
                <!-- END RECENT-POSTS -->
            </div>
            <div class="col-md-9 col-sm-9">
                <!-- START PRODUCT-BANNER -->
                <div class="product-banner home1-banner">
                    <div class="row">
                        <div class="col-md-7 banner-box1">
                            <div class="single-product-banner">
                                <a href="#"><img src="img/banner/3.jpg" alt="Product Banner"></a>
                                <div class="banner-text banner-1">
                                    <h2>head phone 2015</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 banner-box2">
                            <div class="single-product-banner">
                                <a href="#"><img src="img/banner/4.jpg" alt="Product Banner"></a>
                                <div class="banner-text banner-2">
                                    <h2>Deals <span>50%</span></h2>
                                    <p>lumina n85</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-BANNER -->



                <!-- START PRODUCT-AREA (2) -->
                <?php
                $i = 0;
                foreach ($categories as $category):
                    ?>
                    <?php if ($category->children): ?>
                        <div class="product-area">
                            <!-- Start Product-Menu -->
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="product-menu  border-red">
                                        <div class="product-title">
                                            <h3 class="title-group-2 gfont-1"><?php echo $category->title; ?></h3>
                                        </div>

                                        <ul role="tablist">
                                            <!-- <li role="presentation" class=" active"><a href="#display-2-1" role="tab" data-toggle="tab">Dell</a></li>
                                            <li role="presentation"><a href="#display-2-2" role="tab" data-toggle="tab">Hp</a></li>
                                            <li role="presentation"><a href="#display-2-3"  role="tab" data-toggle="tab">Apple</a></li> -->
                                            <?php
                                            $j = 0;
                                            foreach ($category->children as $child):
                                                ?>
                                                <li role="presentation" <?php echo($j == 0) ? 'class="active"' : ''; ?>><a href="#<?php echo $child->id; ?>"  role="tab" data-toggle="tab"><?php echo $child->title; ?></a></li>
                                                <?php
                                                $j++;
                                            endforeach;
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End Product-Menu -->
                            <!-- Start Product -->
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="product carosel-navigation">
                                        <div class="tab-content">
                                            <!-- Start Product = display-2-4 -->
                                            <?php
                                            $j = 0;
                                            foreach ($category->children as $child):
                                                ?>
                                                <div role="tabpanel" class="tab-pane fade <?php echo($j == 0) ? 'in active' : ''; ?>" id="<?php echo $child->id; ?>">
                                                    <?php if ($child->products): ?>
                                                        <div class="row">
                                                            <div class="active-product-carosel">
                                                                <!-- Start Single-Product -->
                                                                <?php foreach ($child->getProducts(6) as $product): ?>
                                                                    <div class="col-xs-12">
                                                                        <div class="single-product">
                                                                            <?php if ($product->is_new): ?>
                                                                                <div class="label_new">
                                                                                    <span class="new">new</span>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                            <?php if ($product->old_price > $product->price): ?>
                                                                                <div class="sale-off">
                                                                                    <span class="sale-percent">-<?php echo $product->getDiscount(); ?>%</span>
                                                                                </div>
                                                                            <?php endif; ?>
                                                                            <div class="product-img">
                                                                                <a href="<?php echo Url::to(['/product/default/view', 'id' => $product->id]); ?>">
                                                                                    <img class="primary-img" src="<?php echo $product->getImage(); ?>" alt="Product">
                                                                                    <?php if ($product->secondary_img): ?>
                                                                                        <img class="secondary-img" src="<?php echo $product->secondary_img; ?>" alt="Product">
                                                                                    <?php endif; ?>
                                                                                </a>
                                                                            </div>
                                                                            <div class="product-description">
                                                                                <h5>
                                                                                    <a href="<?php echo Url::to(['/product/default/view', 'id' => $product->id]); ?>">
                                                                                        <?php echo $product->title; ?>																
                                                                                    </a>
                                                                                </h5>
                                                                                <div class="price-box">
                                                                                    <span class="price"><?php echo $product->price; ?></span>
                                                                                    <?php if ($product->old_price > $product->price): ?>
                                                                                        <span class="old-price"><?php echo $product->old_price; ?></span>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                                <span class="rating">
                                                                                    <i class="fa fa-star"></i>
                                                                                    <i class="fa fa-star"></i>
                                                                                    <i class="fa fa-star"></i>
                                                                                    <i class="fa fa-star"></i>
                                                                                    <i class="fa fa-star-o"></i>
                                                                                </span>
                                                                                <div class="product-action">
                                                                                    <div class="button-group">
                                                                                        <div class="product-button">
                                                                                            <a href="<?php echo Url::to(['/cart/default/add', 'id' => $product->id]); ?>" class="add-to-cart" data-id="<?php echo $product->id; ?>">
                                                                                                <i class="fa fa-shopping-cart add-to-cart"></i> 
                                                                                                Add to Cart
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="product-button-2">
                                                                                            <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                                            <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                                            <a href="#" class="modal-view" data-id="<?php echo $product->id; ?>" data-toggle="modal" data-target="#productModal"><i class="fa fa-search-plus"></i></a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>		
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>
                                                    <?php else: ?>
                                                        <p>У этой категории нет продуктов</p>
                                                    <?php endif; ?>
                                                </div>
                                                <?php
                                                $j++;
                                            endforeach;
                                            ?>
                                            <!-- End Product = display-2-4 -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Product -->
                        </div>
                    <?php else: ?>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="product-menu  border-red">
                                    <div class="product-title">
                                        <h3 class="title-group-2 gfont-1"><?php echo $category->title; ?></h3>
                                        <p>Нет дочерних категорий</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php
                    $i++;
                endforeach;
                ?>
                <!-- END PRODUCT-AREA (2) -->











                <!-- START PRODUCT-BANNER -->
                <div class="product-banner">
                    <div class="row">
                        <div class="col-md-7 banner-box1">
                            <div class="single-product-banner">
                                <a href="#"><img src="img/banner/5.jpg" alt="Product Banner"></a>
                                <div class="banner-text banner-1">
                                    <h2>ApBle 4s</h2>
                                    <p>Vibrant colors beautifully designed</p>
                                    <span>$888.66</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 banner-box2">
                            <div class="single-product-banner">
                                <a href="#"><img src="img/banner/6.jpg" alt="Product Banner"></a>
                                <div class="banner-text banner-2">
                                    <h2>Htc <span>N8.</span></h2>
                                    <p>lumina n85</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-BANNER -->
                <!-- START  -->
                <!-- START SMALL-PRODUCT-AREA (1) -->
                <div class="small-product-area">
                    <!-- Start Product-Menu -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product-menu">
                                <ul role="tablist">
                                    <li role="presentation" class=" active"><a href="#display-4-1" role="tab" data-toggle="tab">Latest</a></li>
                                    <li role="presentation"><a href="#display-4-2" role="tab" data-toggle="tab">Sale</a></li>
                                    <li role="presentation"><a href="#display-4-3"  role="tab" data-toggle="tab">Random</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carosel-navigation">
                                <div class="tab-content">
                                    <!-- Product = display-4-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-4-1">
                                        <div class="row">
                                            <div class="active-small-product">

                                                <?php $i = 0;
                                                while ($i < count($latestProducts)):
                                                    ?>
                                                    <div class="col-xs-12">
                                                        <?php $j = 0;
                                                        while ($j < 3):
                                                            ?>
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="<?php echo Url::to(['product/default/view', 'id' => $latestProducts[$i]->id]); ?>">
        <?php echo Html::img("{$latestProducts[$i]->getImage()}", ['alt' => 'Product', 'class' => 'primary-img', 'width' => '100px']); ?>
                                                                    </a>
                                                                </div>
                                                                <div class="product-description">
                                                                    <h5><a href="<?php echo Url::to(['product/default/view', 'id' => $latestProducts[$i]->id]); ?>"><?php echo $latestProducts[$i]->title; ?></a></h5>
                                                                    <div class="price-box">
                                                                        <span class="price">$<?php echo $latestProducts[$i]->price; ?></span>
                                                                        <?php if ($latestProducts[$i]->old_price > $latestProducts[$i]->price): ?>
                                                                            <span class="old-price">$<?php echo $latestProducts[$i]->old_price; ?></span>
        <?php endif; ?>
                                                                    </div>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                    <div class="product-action">
                                                                        <div class="product-button-2">
                                                                            <a href="<?php echo Url::to(['/cart/default/add', 'id' => $latestProducts[$i]->id]); ?>" class="add-to-cart" data-id="<?php echo $latestProducts[$i]->id; ?>" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                            <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php $i++; ?>
                                                        <?php $j++; ?>
                                                    <?php endwhile; ?>
                                                    </div>
<?php endwhile; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-1 -->

                                    <!-- Product = display-4-2 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-4-2">
                                        <div class="row">
                                            <div class="active-small-product">

                                                    <?php $i = 0;
                                                    while ($i < count($saleProducts)):
                                                        ?>
                                                    <div class="col-xs-12">
    <?php $j = 0;
    while ($j < 3):
        ?>
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="<?php echo Url::to(['product/default/view', 'id' => $saleProducts[$i]->id]); ?>">
        <?php echo Html::img("{$saleProducts[$i]->getImage()}", ['alt' => 'Product', 'class' => 'primary-img', 'width' => '100px']); ?>
                                                                    </a>
                                                                </div>
                                                                <div class="product-description">
                                                                    <h5><a href="<?php echo Url::to(['product/default/view', 'id' => $saleProducts[$i]->id]); ?>"><?php echo $saleProducts[$i]->title; ?></a></h5>
                                                                    <div class="price-box">
                                                                        <span class="price">$<?php echo $saleProducts[$i]->price; ?></span>
        <?php if ($saleProducts[$i]->old_price > $saleProducts[$i]->price): ?>
                                                                            <span class="old-price">$<?php echo $saleProducts[$i]->old_price; ?></span>
        <?php endif; ?>
                                                                    </div>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                    <div class="product-action">
                                                                        <div class="product-button-2">
                                                                            <a href="<?php echo Url::to(['/cart/default/add', 'id' => $saleProducts[$i]->id]); ?>" class="add-to-cart" data-id="<?php echo $saleProducts[$i]->id; ?>" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                            <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php $i++; ?>
        <?php $j++; ?>
    <?php endwhile; ?>
                                                    </div>
<?php endwhile; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-2 -->

                                    <!-- Product = display-4-3 -->
                                    <div role="tabpanel" class="tab-pane fade" id="display-4-3">
                                        <div class="row">
                                            <div class="active-small-product">

                                                    <?php $i = 0;
                                                    while ($i < count($randomProducts)):
                                                        ?>
                                                    <div class="col-xs-12">
                                                                    <?php $j = 0;
                                                                    while ($j < 3):
                                                                        ?>
                                                            <div class="single-product">
                                                                <div class="product-img">
                                                                    <a href="<?php echo Url::to(['product/default/view', 'id' => $randomProducts[$i]->id]); ?>">
        <?php echo Html::img("{$randomProducts[$i]->getImage()}", ['alt' => 'Product', 'class' => 'primary-img', 'width' => '100px']); ?>

                                                                    </a>
                                                                </div>
                                                                <div class="product-description">
                                                                    <h5><a href="<?php echo Url::to(['product/default/view', 'id' => $randomProducts[$i]->id]); ?>"><?php echo $randomProducts[$i]->title; ?></a></h5>
                                                                    <div class="price-box">
                                                                        <span class="price">$<?php echo $randomProducts[$i]->price; ?></span>
        <?php if ($randomProducts[$i]->old_price > $randomProducts[$i]->price): ?>
                                                                            <span class="old-price">$<?php echo $randomProducts[$i]->old_price; ?></span>
        <?php endif; ?>
                                                                    </div>
                                                                    <span class="rating">
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star"></i>
                                                                        <i class="fa fa-star-o"></i>
                                                                    </span>
                                                                    <div class="product-action">
                                                                        <div class="product-button-2">
                                                                            <a href="<?php echo Url::to(['/cart/default/add', 'id' => $randomProducts[$i]->id]); ?>" class="add-to-cart" data-id="<?php echo $randomProducts[$i]->id; ?>" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
                                                                            <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
        <?php $i++; ?>
        <?php $j++; ?>
    <?php endwhile; ?>
                                                    </div>
<?php endwhile; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-3 -->										
                                </div>
                            </div>									
                        </div>
                    </div>
                    <!-- End Product -->
                </div>
                <!-- END SMALL-PRODUCT-AREA (1) -->
            </div>
        </div>
    </div>
    <?php
    $this->registerJsFile('@web/js/product-modal.js', [
        'depends' => JqueryAsset::class
    ]);
    ?>