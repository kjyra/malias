<?php 
use frontend\widgets\CategoryWidget;
?>
    <!-- CATEGORY-MENU-LIST START -->
    <div class="left-category-menu hidden-sm hidden-xs">
        <div class="left-product-cat">
            <div class="category-heading">
                <h2>categories</h2>
            </div>
            <div class="category-menu-list">
                <!-- categories widget -->
                <?php echo CategoryWidget::widget(); ?>
            </div>
        </div>
    </div>
    <!-- END CATEGORY-MENU-LIST -->
