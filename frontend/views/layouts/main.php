<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\Nav;
use frontend\widgets\search\SearchWidget;

//session_destroy();
//debug($_SESSION);
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <base href="/" >
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <!-- HEADER-AREA START -->
        <header class="header-area">
            <!-- HEADER-TOP START -->
            <div class="header-top hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="top-menu">
                                <!-- Start Language -->
                                <ul class="language">
                                    <li><a href="#"><?php echo Yii::$app->language; ?><i class="fa fa-caret-down left-5"></i></a>
                                        <ul>
                                                <li><a href="#" class="right-5" data-language="ru-RU"><img class="right-5" src="img/flags/fr.png" alt="#">Russian</a></li>
                                                <li><a href="#" class="right-5" data-language="en-US"><img class="right-5" src="img/flags/gb.png" alt="#">English</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- End Language -->
                                <p class="welcome-msg">Default welcome msg!</p>
                            </div>
                            <!-- Start Top-Link -->
                            <div class="top-link">
                                <ul class="link">
                                    <?php if (!Yii::$app->user->isGuest): ?>
                                        <li><a href="my-account.html"><i class="fa fa-user"></i> <?php echo Yii::t('main', 'My Account'); ?></a></li>
                                    <?php endif; ?>
                                    <li><a href="<?php echo Url::to(['/cart/default/checkout']); ?>"><i class="fa fa-share"></i> <?php echo Yii::t('main', 'Checkout'); ?></a></li>
                                    <?php if (Yii::$app->user->isGuest): ?>
                                        <li><a href="<?php echo Url::to(['/user/default/login']); ?>"><i class="fa fa-unlock-alt"></i> <?php echo Yii::t('main', 'Login'); ?></a></li>
                                        <li><a href="<?php echo Url::to(['/user/default/signup']); ?>"><i class="fa fa-lock"></i> <?php echo Yii::t('main', 'Signup'); ?></a></li>
                                    <?php else: ?>
                                        <li><a href="<?php echo Url::to(['/user/default/logout']); ?>"><i class="fa fa-sign-out"></i> 
                                            <?php echo Yii::t('main', 'Logout ({username})',
                                                [
                                                    'username' => Yii::$app->user->identity->username                                                    
                                                ]); ?>
                                            </a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <!-- End Top-Link -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- HEADER-TOP END -->
            <!-- HEADER-MIDDLE START -->
            <div class="header-middle">
                <div class="container">
                    <!-- Start Support-Client -->
                    <div class="support-client hidden-xs">
                        <div class="row">
                            <!-- Start Single-Support -->
                            <div class="col-md-3 hidden-sm">
                                <div class="single-support">
                                    <div class="support-content">
                                        <i class="fa fa-clock-o"></i>
                                        <div class="support-text">
                                            <h1 class="zero gfont-1">working time</h1>
                                            <p>Mon- Sun: 8.00 - 18.00</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Support -->
                            <!-- Start Single-Support -->
                            <div class="col-md-3 col-sm-4">
                                <div class="single-support">
                                    <i class="fa fa-truck"></i>
                                    <div class="support-text">
                                        <h1 class="zero gfont-1">Free shipping</h1>
                                        <p>On order over $199</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Support -->
                            <!-- Start Single-Support -->
                            <div class="col-md-3 col-sm-4">
                                <div class="single-support">
                                    <i class="fa fa-money"></i>
                                    <div class="support-text">
                                        <h1 class="zero gfont-1">Money back 100%</h1>
                                        <p>Within 30 Days after delivery</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Support -->
                            <!-- Start Single-Support -->
                            <div class="col-md-3 col-sm-4">
                                <div class="single-support">
                                    <i class="fa fa-phone-square"></i>
                                    <div class="support-text">
                                        <h1 class="zero gfont-1">Phone: 0123456789</h1>
                                        <p>Order Online Now !</p>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single-Support -->
                        </div>
                    </div>
                    <!-- End Support-Client -->
                    <!-- Start logo & Search Box -->
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="logo">
                                <a href="<?= Url::to(['/']); ?>" title="Malias"><img src="img/logo.png" alt="Malias"></a>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-12">
                            <div class="quick-access">
<!--                                <div class="search-by-category">
                                    <div class="search-container">
                                        <select>
                                            <option class="all-cate">All Categories</option>
                                            <optgroup  class="cate-item-head" label="Cameras & Photography">
                                                <option class="cate-item-title">Handbags</option>
                                                <option class="c-item">Blouses And Shirts</option>
                                                <option class="c-item">Clouthes</option>
                                            </optgroup>
                                            <optgroup  class="cate-item-head" label="Laptop & Computer">
                                                <option class="cate-item-title">Apple</option>
                                                <option class="c-item">Dell</option>
                                                <option class="c-item">Hp</option>
                                                <option class="c-item">Sony</option>
                                            </optgroup>
                                            <optgroup  class="cate-item-head" label="Electronic">
                                                <option class="c-item">Mobile</option>
                                                <option class="c-item">Speaker</option>
                                                <option class="c-item">Headphone</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <div class="header-search">
                                        <form action="#">
                                            <input type="text" placeholder="Search">
                                            <button type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                </div>-->
                                <?= SearchWidget::widget() ?>
                                <div class="top-cart">
                                    <ul>
                                        <li>
                                            <a href="<?php echo Url::to(['/cart/default/view']); ?>">
                                                <span class="cart-icon"><i class="fa fa-shopping-cart"></i></span>
                                                <span class="cart-total">
                                                    <span class="cart-title">shopping cart</span>
                                                        <span class="cart-item">
                                                                <?php if(Yii::$app->user->isGuest): ?>
                                                                    <?php echo $_SESSION['qty'] ?? '0'; ?> item(s)- 
                                                                <?php else: ?>
                                                                    <?php echo frontend\models\Cartdb::getTotalQty() ?? '0'; ?> item(s)-
                                                                <?php endif; ?>
                                                        </span>
                                                        <span class="top-cart-price">
                                                                <?php if(Yii::$app->user->isGuest): ?>
                                                                    $<?php echo $_SESSION['sum'] ?? '0'; ?> 
                                                                <?php else: ?>
                                                                    $<?php echo frontend\models\Cartdb::getTotalSum() >> '0'; ?>
                                                                <?php endif; ?>
                                                        </span>
                                                </span>
                                            </a>
                                            <div class="mini-cart-content">
                                                
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End logo & Search Box -->
                </div> 
            </div>
            <!-- HEADER-MIDDLE END -->
            <!-- START MAINMENU-AREA -->
            <div class="mainmenu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mainmenu hidden-sm hidden-xs">
                                <nav>
                                    <?php
                                    $menuItems = [
                                        ['label' => Yii::t('main', 'Home'), 'url' => ['/site/index']],
                                        ['label' => Yii::t('main', 'About us'), 'url' => ['/site/about']],
                                        ['label' => Yii::t('main', 'Bestseller Products'), 'url' => [''], 'options' => ['class' => 'hot']],
                                        ['label' => Yii::t('main', 'New Products'), 'url' => [''], 'options' => ['class' => 'new']],
                                        ['label' => Yii::t('main', 'Special Products')],
                                        ['label' => Yii::t('main', 'Pages'), 'url' => [''], 'items' => [
                                                ['label' => 'Cart', 'url' => Url::to(['/cart/default/view'])], // второй уровень
                                                ['label' => 'Checkout', 'url' => Url::to(['/cart/default/checkout'])],
                                                ['label' => 'Create Account', 'url' => Url::to(['/user/default/signup'])],
                                                ['label' => 'My Accout', 'url' => Url::to(['/'])],
                                                ['label' => 'Product details', 'url' => Url::to(['/'])],
                                                ['label' => 'Shop Grid View', 'url' => Url::to(['/'])],
                                                ['label' => 'Shop List View', 'url' => Url::to(['/'])],
                                                ['label' => 'Wish List', 'url' => Url::to(['/'])],
                                            ], 'options' => ['class' => '']],
                                        ['label' => Yii::t('main', 'Contact Us'), 'url' => ['']],
                                    ];

                                    echo Nav::widget([
                                        'options' => ['class' => ''],
                                        'items' => $menuItems,
                                    ]);
                                    ?>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN-MENU-AREA -->
            <!-- Start Mobile-menu -->
            <div class="mobile-menu-area hidden-md hidden-lg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <nav id="mobile-menu">
                                <ul>
                                    <li><a href="index.html">Home</a>
                                        <ul>
                                            <li><a href="index.html">Home Page 1</a></li>
                                            <li><a href="index-2.html">Home Page 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="shop.html">Bestseller Products</a></li>
                                    <li><a href="shop-list.html">New Products</a></li>
                                    <li><a href="#">Pages</a>
                                        <ul>
                                            <li><a href="cart.html">Cart</a></li>
                                            <li><a href="checkout.html">Checkout</a></li>
                                            <li><a href="account.html">Create Account</a></li>
                                            <li><a href="login.html">Login</a></li>
                                            <li><a href="my-account.html">My Account</a></li>
                                            <li><a href="product-details.html">Product details</a></li>
                                            <li><a href="shop.html">Shop Grid View</a></li>
                                            <li><a href="shop-list.html">Shop List View</a></li>
                                            <li><a href="wishlist.html">Wish List</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Mobile-menu -->
        </header>
        <!-- HEADER AREA END -->
        <?= Alert::widget(); ?>
        <?= $content; ?>
        <div id="quickview-wrapper">
            <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">

                        </div> 
                    </div>
                </div>
            </div>
        </div>

                    <!-- START BRAND-LOGO-AREA -->
                    <div class="brand-logo-area carosel-navigation">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="area-title">
                                        <h3 class="title-group border-red gfont-1">Brand Logo</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="active-brand-logo">
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/4.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/5.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/6.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="single-brand-logo">
                                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END BRAND-LOGO-AREA -->
                    <!-- START SUBSCRIBE-AREA -->
                    <div class="subscribe-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-7 col-xs-12">
                                    <label class="hidden-sm hidden-xs">Sign Up for Our Newsletter:</label>
                                    <div class="subscribe">
                                        <form action="#">
                                            <input type="text" placeholder="Enter Your E-mail">
                                            <button type="submit">Subscribe</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-5 col-xs-12">
                                    <div class="social-media">
                                        <a href="#"><i class="fa fa-facebook fb"></i></a>
                                        <a href="#"><i class="fa fa-google-plus gp"></i></a>
                                        <a href="#"><i class="fa fa-twitter tt"></i></a>
                                        <a href="#"><i class="fa fa-youtube yt"></i></a>
                                        <a href="#"><i class="fa fa-linkedin li"></i></a>
                                        <a href="#"><i class="fa fa-rss rs"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>	
                    <!-- END SUBSCRIBE-AREA -->
                    </section>
                    <!-- END PAGE-CONTENT -->
                    <!-- FOOTER-AREA START -->
                    <footer class="footer-area">
                        <!-- Footer Start -->
                        <div class="footer">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <div class="footer-title">
                                            <h5>My Account</h5>
                                        </div>
                                        <nav>
                                            <ul class="footer-content">
                                                <li><a href="my-account.html">My Account</a></li>
                                                <li><a href="#">Order History</a></li>
                                                <li><a href="wishlist">Wish List</a></li>
                                                <li><a href="#">Search Terms</a></li>
                                                <li><a href="#">Returns</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <div class="footer-title">
                                            <h5>Customer Service</h5>
                                        </div>
                                        <nav>
                                            <ul class="footer-content">
                                                <li><a href="contact.html">Contact Us</a></li>
                                                <li><a href="about.html">About Us</a></li>
                                                <li><a href="#">Delivery Information</a></li>
                                                <li><a href="#">Privacy Policy</a></li>
                                                <li><a href="#">Terms & Conditions</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="col-xs-12 hidden-sm col-md-3">
                                        <div class="footer-title">
                                            <h5>Payment & Shipping</h5>
                                        </div>
                                        <nav>
                                            <ul class="footer-content">
                                                <li><a href="#">Brands</a></li>
                                                <li><a href="#">Gift Vouchers</a></li>
                                                <li><a href="#">Affiliates</a></li>
                                                <li><a href="shop-list.html">Specials</a></li>
                                                <li><a href="#">Search Terms</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <div class="footer-title">
                                            <h5>Payment & Shipping</h5>
                                        </div>
                                        <nav>
                                            <ul class="footer-content box-information">
                                                <li>
                                                    <i class="fa fa-home"></i><span>Towerthemes, 1234 Stret Lorem, LPA States, Libero</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-envelope-o"></i><p><a href="contact.html">admin@bootexperts.com</a></p>
                                                </li>
                                                <li>
                                                    <i class="fa fa-phone"></i>
                                                    <span>01234-56789</span> <br> <span> 01234-56789</span>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>				
                        </div>
                        <!-- Footer End -->
                        <!-- Copyright-area Start -->
                        <div class="copyright-area">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="copyright">
                                            <p>Copyright &copy; Взято с <a href="http://bayguzin.ru" target="_blank"> bayguzin.ru</a> All rights reserved.</p>
                                            <div class="payment">
                                                <a href="#"><img src="img/payment.png" alt="Payment"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Copyright-area End -->
                    </footer>
                    <div class="preloader"><img src="img/ring.svg" alt=""></div>
                    <!-- FOOTER-AREA END -->	
                    <!-- QUICKVIEW PRODUCT -->
                    <!-- END QUICKVIEW PRODUCT -->
                    <?php $this->endBody() ?>
                    </body>
                    </html>
                    <?php $this->endPage() ?>