<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use frontend\models\ContactForm;
use frontend\modules\product\models\ProductFindModel;
use frontend\modules\category\models\CategoryFindModel;
use yii\web\Cookie;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $categoriesTitle = ['Cameras & Photography', 'Tv & Audio', 'Blouses And Shirts'];
        $categories = CategoryFindModel::findCategoriesByTitle($categoriesTitle);
        
        $latestProducts = ProductFindModel::findLatestProducts();
        $randomProducts = ProductFindModel::findRandomProducts();
        $saleProducts = ProductFindModel::findSaleProducts();
        
        
        return $this->render('index', [
            'categories' => $categories,
            
            'latestProducts' => $latestProducts,
            'randomProducts' => $randomProducts,
            'saleProducts' => $saleProducts,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionLanguage()
    {
        $language = Yii::$app->request->post('language');
        if($language && in_array($language, Yii::$app->params['languages'])) {
            Yii::$app->language = $language;

            $languageCookie = new Cookie([
                'name' => 'language',
                'value' => $language,
                'expire' => time() + 60 * 60 * 24 * 30, // 30 days
            ]);

            Yii::$app->response->cookies->add($languageCookie);
            return $this->redirect(Yii::$app->request->referrer);    
        }
        return false;        
    }
    
    public function actionSearch()
    {
        $model = new \frontend\widgets\search\forms\SearchForm();

        if($model->load(Yii::$app->request->post())) {
            $result = $model->search();
        }
        return $this->render('search', [
            'model' => $model,
            'result' => $result
        ]);
    }

}
