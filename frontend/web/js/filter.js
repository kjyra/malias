$('body').on('change', '.filter-menu input', function() {
    var checked = $('.filter-menu input:checked'),
        data = '';
    checked.each(function() {
        data += this.value + ',';
    });
    if(data) {
        $.ajax({
           url: location.href,
           data: {filter: data},
           type: 'GET',
           
           beforeSend: function() {
               $('.preloader').fadeIn(300, function() {
                   $('.product-area').hide();
               });
           },
           
           success: function(res) {
               $('.preloader').delay(500).fadeOut('slow', function() {
                    $('.product-area').html(res).fadeIn();
                    var url = location.search.replace(/filter(.+?)(&|$)/g, '');
                    var newUrl = location.pathname + url + (location.search ? "&" : "?") + "filter=" + data;
                    newUrl = newUrl.replace('&&', '&');
                    newUrl = newUrl.replace('?&', '?');
                    history.pushState({}, '', newUrl);
               });
           },
           error: function() {
               
           }
        });
    } else {
        window.location = location.pathname
    }
});