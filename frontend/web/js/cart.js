function showCart(cart) {
    $('.top-cart li').html(cart);
}

function getCart() {
    $.get('cart/default/show', {}, function(data) {
        showCart(data);
    });
}

$(document).ready(function() {
    $('.add-to-cart').click(function() {
        var params = {
            'id': $(this).data('id')
        };
        $.get('cart/default/add', params, function(data) {
            showCart(data);
        });
        return false;
    });
    
    $('.top-cart').one('mouseenter', function() {
        getCart();
        return;
    });
    
    $('.update-qty').click(function() {
        var params = {
            'id': $(this).data('id'),
            'qty': $(this).parent().siblings('.get-qty').val()
        };
        console.log(params);
        $.get('cart/default/update', params, function() {
            location = 'cart';
//            console.log(data);
        });
    });
    
    $('.remove-product').click(function() {
        var params = {
            'id': $(this).data('id')
        }
        
        $.get('cart/default/delete', params, function() {
            location = 'cart';
        });
    });
});
