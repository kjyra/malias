$(document).on('change', '#sort', function () {
    var sort = $(this).val()

    var url = location.search.replace(/sort(.+?)(&|$)/g, '');
    var newUrl = location.pathname + url + (location.search ? "&" : "?") + "sort=" + sort;
    newUrl = newUrl.replace('&&', '&');
    newUrl = newUrl.replace('?&', '?');
    history.pushState({}, '', newUrl);

    $.ajax({
        url: location.href,
        data: {'sort': sort},
        type: 'GET',

        beforeSend: function () {
            $('.preloader').fadeIn(300, function () {
                $('.product-area').hide();
            });
        },

        success: function (res) {
            $('.preloader').delay(500).fadeOut('slow', function () {
                $('.product-area').html(res).fadeIn();
            });
        },

        error: function () {

        }

    });
});