$(document).ready(function() {
    $('.modal-view').on('click', function () {
        var params = {
            'id': $(this).data('id')
        }
        $.get('/product/default/product-modal', params, function(data) {
            $('#productModal .modal-body').html(data);
            $('#productModal').modal();
        });
    });

    $('#productModal').on('hidden.bs.modal', function (e) {
        $('#productModal').modal('hide');
    });
    
});



