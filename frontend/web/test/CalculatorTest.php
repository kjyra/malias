<?php

namespace test;

require 'calculator.php';

/**
 * Description of CalculatorTest
 *
 * @author grigo
 */
class CalculatorTest
{
    
    public function __construct()
    {
        self::testAddCorrect();
        self::testMult();
        self::testDivideCorrect();
        self::testDivideZero();
    }
    
    public static function testAddCorrect()
    {
        echo 'Running: ' . __METHOD__ . '<br>';
        
        $result = calculator::add(5, 2);
        
        if($result == 7) {
            echo 'Passed';
        } else {
            echo "Failed: expected 7. Result: (" . gettype($result) . ") $result";
        }
        
        echo '<hr>';
    }
    
    public static function testMult()
    {
        echo 'Running: ' . __METHOD__ . '<br>';
        
        $result = calculator::mult(3, 4);
        if($result == 12) {
            echo 'Passed';
        } else {
            echo "Failed: expected 12. Result: (" . gettype($result) . ") $result";
        }
        
        echo '<hr>';
    }
    
    public static function testDivideCorrect()
    {
        echo 'Running: ' . __METHOD__ . '<br>';
        
        $result = calculator::divide(4, 2);
        if($result == 2) {
            echo 'Passed';
        } else {
            echo "Failed: expected 2. Result: (" . gettype($result) . ") $result";
        }
        
        echo '<hr>';
    }
    
    public static function testDivideZero()
    {
        echo 'Running: ' . __METHOD__ . '<br>';
        
        $result = calculator::divide(4, 0);
        if($result === null) {
            echo 'Passed';
        } else {
            echo "Failed: expected NULL. Result: (" . gettype($result) . ") $result";
        }
        
        echo '<hr>';
    }
    
}
