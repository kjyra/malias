<?php

namespace test;

/**
 * Description of calculator
 *
 * @author grigo
 */
class calculator 
{
    
    public static function add($a, $b)
    {
        return $a + $b;
    }
    
    public static function mult($a, $b)
    {
       return $a * $b; 
    }
    
    public static function divide($a, $b)
    {
        if($b == 0) {
            return null;
        }
        return $a / $b;
    }
}
