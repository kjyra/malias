<?php

return [
    'Login' => 'Войти',
    'Signup' => 'Зарегистрироваться',
    'Logout ({username})' => 'Выйти ({username})',
    'My Account' => 'Мой Аккаунт',
    'Checkout' => 'Оформить заказ',
    
    'Home' => 'Главная',
    'About us' => 'О нас',
    'Bestseller Products' => 'Продукты Бестселлеры',
    'New Products' => 'Новые Продукты',
    'Special Products' => 'Особые Продукты',
    'Pages' => 'Страницы',
    'Contact Us' => 'Напишите Нам',
];

